package com.asilarslan.wordpress.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.asilarslan.wordpress.R;
import com.asilarslan.wordpress.activity.AuthorPostsActivity;
import com.asilarslan.wordpress.activity.PostDetailActivity;
import com.asilarslan.wordpress.model.Author;
import com.asilarslan.wordpress.model.Post;
import com.asilarslan.wordpress.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AuthorAdapter extends RecyclerView.Adapter<AuthorViewHolder> {

    private Context mContext;
    private List<Author> mPostList;

    public AuthorAdapter(Context mContext, List<Author> mPostList) {
        this.mContext = mContext;
        this.mPostList = mPostList;
    }

    @Override
    public AuthorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_author_row_item, parent, false);
        return new AuthorViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(AuthorViewHolder holder, int position) {
//        final Ad ad = mAdList.get(position);
//        holder.title.setText(ad.getTitle());
//        if (ad.getImages().size() > 0 ){
//            Picasso.get().load(ad.getImages().get(0)).into(holder.imageView);
//        }
        //holder.excerpt.setText(ad.getCity().getName());
//        holder.price.setText(Util.currencyFormat(ad.getPrice()));




        final Author post = mPostList.get(position);
        holder.title.setText(Html.fromHtml(post.getFirst_name() + " " + post.getLast_name()));
//        holder.date.setText(Utils.formatDate("yyyy-MM-dd'T'HH:mm:ss", "dd MMM yyyy",post.getDate()));
        if(post.getPhoto() != null){
            Picasso.get().load(post.getPhoto()).into(holder.imageView);
        }else{
            holder.imageView.setVisibility(View.GONE);
        }

        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int dpWidth = (int) (displayMetrics.widthPixels / displayMetrics.density);

        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int dimensionInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, displayMetrics);

        holder.itemView.getLayoutParams().width = width - width / 2;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(mContext, AuthorPostsActivity.class);
                intent.putExtra("author", post);
                mContext.startActivity(intent);

            }
        });
    }



    @Override
    public int getItemCount() {
        return mPostList.size();
    }
}

class AuthorViewHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public ImageView imageView;
    public TextView date;
    AuthorViewHolder(View itemView) {
        super(itemView);

        title = (TextView)itemView.findViewById(R.id.title);
        imageView = (ImageView) itemView.findViewById(R.id.imagev);
        date = (TextView)itemView.findViewById(R.id.date);
//        price = (TextView)itemView.findViewById(R.id.price);
    }
}
