package com.asilarslan.wordpress.ui.favorite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asilarslan.wordpress.R;
import com.asilarslan.wordpress.adapter.PostAdapter;
import com.asilarslan.wordpress.helper.DatabaseHelper;
import com.asilarslan.wordpress.model.Post;

import java.util.ArrayList;
import java.util.List;

public class FavoriteFragment extends Fragment {


    private PostAdapter postAdapter;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        RecyclerView mRecyclerView = root.findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        DatabaseHelper db = new DatabaseHelper(getActivity());
        postAdapter = new PostAdapter(getContext(), db.getFavoriteList());
        mRecyclerView.setAdapter(postAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);


        return root;
    }
}