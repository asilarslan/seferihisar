package com.asilarslan.wordpress.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.asilarslan.wordpress.Config;
import com.asilarslan.wordpress.R;
import com.asilarslan.wordpress.adapter.AuthorAdapter;
import com.asilarslan.wordpress.adapter.HeaderAdapter;
import com.asilarslan.wordpress.adapter.PostAdapter;
import com.asilarslan.wordpress.model.Author;
import com.asilarslan.wordpress.model.Post;
import com.asilarslan.wordpress.service.AppController;
import com.google.android.material.progressindicator.CircularProgressIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private HeaderAdapter headerAdapter;
    private AuthorAdapter authorAdapter;
    private PostAdapter postAdapter;
    private List<Post> mHeaderPostList;
    private List<Post> mPostList;
    private List<Author> mAuthorList;
    private CircularProgressIndicator progressBar;
    private TextView headlineView, authorView, latestView;
    private RecyclerView mRecyclerView, recyclerViewAuthor;
    private int page = 1;
    private boolean isLoading = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        progressBar = root.findViewById(R.id.progress_circular);
        headlineView = root.findViewById(R.id.headlineView);
        authorView = root.findViewById(R.id.authorView);
        latestView = root.findViewById(R.id.latestView);
        authorView.setVisibility(View.GONE);
        headlineView.setVisibility(View.GONE);
        latestView.setVisibility(View.GONE);
        // set up the RecyclerView
        RecyclerView recyclerView = root.findViewById(R.id.recyclerviewHeader);
        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        mHeaderPostList = new ArrayList<>();
        headerAdapter = new HeaderAdapter(getContext(),mHeaderPostList);
        recyclerView.setAdapter(headerAdapter);



        recyclerViewAuthor = root.findViewById(R.id.recyclerviewAuthor);
        recyclerViewAuthor.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mAuthorList = new ArrayList<>();
        authorAdapter = new AuthorAdapter(getContext(),mAuthorList);
        recyclerViewAuthor.setAdapter(authorAdapter);


        mRecyclerView = root.findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPostList = new ArrayList<>();
        postAdapter = new PostAdapter(getContext(), mPostList);
        mRecyclerView.setAdapter(postAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        NestedScrollView scroll = (NestedScrollView) root.findViewById(R.id.nestedScrollView);
        scroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) scroll.getChildAt(scroll.getChildCount() - 1);

                int diff = (view.getBottom() - (scroll.getHeight() + scroll
                        .getScrollY()));

                if (diff == 0) {
                    Log.d("last","last");
                    if(mPostList.size() > 0){
                        page = page + 1;
                        getPosts();
                    }
                }
            }
        });


        getHeaderPosts();
        getAuthors();
        getPosts();


        return root;
    }

    // Declare Context variable at class level in Fragment
    private Context mContext;

    // Initialise it from onAttach()
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    private void getHeaderPosts() {
        String url = Config.URL_HEADLINE_POSTS ;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject jsonObjectPost = jsonArray.getJSONObject(i);

                                int id = jsonObjectPost.getInt("id");
                                String title = jsonObjectPost.getJSONObject("title").getString("rendered");
                                String excerpt = jsonObjectPost.getJSONObject("excerpt").getString("rendered");
                                String content = jsonObjectPost.getJSONObject("content").getString("rendered");
                                String date = jsonObjectPost.getString("date");
                                String link = jsonObjectPost.getString("link");
                                String featuredMedia = null;
                                if(!jsonObjectPost.getJSONObject("_embedded").isNull("wp:featuredmedia"))
                                {
                                    featuredMedia = jsonObjectPost.getJSONObject("_embedded").getJSONArray("wp:featuredmedia").getJSONObject(0).getString("source_url");
                                }

                                Post post = new Post(id, title,excerpt,content,featuredMedia,date,link);
                                mHeaderPostList.add(post);
                            }
                            headlineView.setVisibility(View.VISIBLE);
                            progressBar.hide();
                            headerAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("response",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
                // mTextView.setText("That didn't work!");
            }
        });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }



    private void getAuthors() {
        isLoading = true;
        String url = "http://seferihisar.com/mobile/api/get_authors.php";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject jsonObjectPost = jsonArray.getJSONObject(i);

                                int id = jsonObjectPost.getInt("id");
                                String photo = jsonObjectPost.getString("photo");
                                String first_name = jsonObjectPost.getString("first_name");
                                String last_name = jsonObjectPost.getString("last_name");
                                String post_title = jsonObjectPost.getString("post_title");
                                Author author = new Author(id, photo, first_name, last_name, post_title);

                                mAuthorList.add(author);
                            }
                            authorView.setVisibility(View.VISIBLE);
                            authorAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("response",response);
                        // Yanıt dizesinin ilk 500 karakterini görüntüleniyor.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
                // mTextView.setText("That didn't work!");
            }
        });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getPosts() {
        isLoading = true;
        String url = Config.URL_POSTS + "&page=" + String.valueOf(page);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject jsonObjectPost = jsonArray.getJSONObject(i);

                                int id = jsonObjectPost.getInt("id");
                                String title = jsonObjectPost.getJSONObject("title").getString("rendered");
                                String excerpt = jsonObjectPost.getJSONObject("excerpt").getString("rendered");
                                String content = jsonObjectPost.getJSONObject("content").getString("rendered");
                                String date = jsonObjectPost.getString("date");
                                String link = jsonObjectPost.getString("link");
                                String featuredMedia = null;
                                if(!jsonObjectPost.getJSONObject("_embedded").isNull("wp:featuredmedia"))
                                {
                                    featuredMedia = jsonObjectPost.getJSONObject("_embedded").getJSONArray("wp:featuredmedia").getJSONObject(0).getString("source_url");
                                }

                                Post post = new Post(id, title,excerpt,content,featuredMedia,date,link);
                                mPostList.add(post);
                            }
                            latestView.setVisibility(View.VISIBLE);
                            progressBar.hide();
                            postAdapter.notifyDataSetChanged();
                            isLoading = false;


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("response",response);
                        // Yanıt dizesinin ilk 500 karakterini görüntüleniyor.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
                // mTextView.setText("That didn't work!");
            }
        });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}