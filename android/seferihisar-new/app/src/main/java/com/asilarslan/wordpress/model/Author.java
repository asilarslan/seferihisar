package com.asilarslan.wordpress.model;

import java.io.Serializable;

public class Author implements Serializable {

    private int id;
    private String photo;
    private String first_name;
    private String last_name;
    private String post_title;

    public Author(int id, String photo, String first_name, String last_name, String post_title) {
        this.id = id;
        this.photo = photo;
        this.first_name = first_name;
        this.last_name = last_name;
        this.post_title = post_title;
    }

    public int getId() {
        return id;
    }

    public String getPhoto() {
        return photo;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPost_title() {
        return post_title;
    }
}
