package com.asilarslan.wordpress.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.asilarslan.wordpress.Config;
import com.asilarslan.wordpress.R;
import com.asilarslan.wordpress.adapter.PostAdapter;
import com.asilarslan.wordpress.model.Author;
import com.asilarslan.wordpress.model.Post;
import com.asilarslan.wordpress.service.AppController;
import com.google.android.material.progressindicator.CircularProgressIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AuthorPostsActivity extends AppCompatActivity {

    private PostAdapter postAdapter;
    private CircularProgressIndicator progressBar;
    private RecyclerView mRecyclerView;
    private List<Post> mPostList;
    private int page = 1;
    private boolean isLoading = false;
    private Author author;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author_posts);

        author = (Author) getIntent().getSerializableExtra("author");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(author.getFirst_name() + " " + author.getLast_name());

        progressBar = findViewById(R.id.progress_circular);

        mRecyclerView = findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPostList = new ArrayList<>();
        postAdapter = new PostAdapter(this, mPostList);
        mRecyclerView.setAdapter(postAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        getPosts();
    }

    private void getPosts() {
        isLoading = true;
        String url = "https://seferihisar.com/api/get_author_posts/?id=" + String.valueOf(author.getId()) + "&page=" + String.valueOf(page);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("posts");

                            for(int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject jsonObjectPost = jsonArray.getJSONObject(i);

                                int id = jsonObjectPost.getInt("id");
                                String title = jsonObjectPost.getString("title");
                                String excerpt = jsonObjectPost.getString("excerpt");
                                String content = jsonObjectPost.getString("content");
                                String date = jsonObjectPost.getString("date");
                                String link = jsonObjectPost.getString("url");
                                String featuredMedia = null;
//                                if(!jsonObjectPost.getJSONObject("_embedded").isNull("wp:featuredmedia"))
//                                {
//                                    featuredMedia = jsonObjectPost.getJSONObject("_embedded").getJSONArray("wp:featuredmedia").getJSONObject(0).getString("source_url");
//                                }

                                Post post = new Post(id, title,excerpt,content,featuredMedia,date,link);
                                mPostList.add(post);
                            }
                            progressBar.hide();
                            postAdapter.notifyDataSetChanged();
                            isLoading = false;


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("response",response);
                        // Yanıt dizesinin ilk 500 karakterini görüntüleniyor.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
                // mTextView.setText("That didn't work!");
            }
        });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigateUp() {
        onBackPressed();
        return true;
    }
}