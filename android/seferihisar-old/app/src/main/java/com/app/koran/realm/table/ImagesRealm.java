package com.app.koran.realm.table;

import com.app.koran.model.Image;
import com.app.koran.model.Images;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ImagesRealm extends RealmObject {

    public ImageRealm full = new ImageRealm();
    public ImageRealm medium = new ImageRealm();
    public ImageRealm medium_large = new ImageRealm();
    public ImageRealm large = new ImageRealm();

    public Images getOriginal() {
        Images t = new Images();
        t.full = (full != null ? full.getOriginal() : null);
        t.medium = (medium != null ? medium.getOriginal() : null);
        t.medium_large = (medium_large != null ? medium_large.getOriginal() : null);
        t.large = (large != null ? large.getOriginal() : null);
        return t;
    }
}
