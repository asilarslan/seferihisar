package com.app.koran.adapter;

import android.widget.*;
import java.util.*;
import android.graphics.*;
import android.view.*;
import android.content.*;

import com.app.koran.ActivityFullScreenImage;
import com.app.koran.ActivityGallery;
import com.app.koran.model.Attachment;

public class ImageAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Bitmap> bitmapList;
    private List<Attachment> attachments;

    public ImageAdapter(Context context, ArrayList<Bitmap> bitmapList, List<Attachment> attachments) {
        this.context = context;
        this.bitmapList = bitmapList;
        this.attachments = attachments;
    }

    public int getCount() {
        return this.bitmapList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.context);
            imageView.setLayoutParams(new GridView.LayoutParams(320, 320));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityFullScreenImage.navigate((ActivityGallery)context, attachments.get(position).images.large.url);
            }
        });

        imageView.setImageBitmap(this.bitmapList.get(position));
        return imageView;
    }

}