package com.app.koran.adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.app.koran.model.Author;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import com.app.koran.R;
import com.app.koran.data.Constant;
import com.app.koran.model.Post;
import com.app.koran.utils.Tools;

public class AdapterPostList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_AD = 3;
    private final int VIEW_HEADER = 2;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<Post> items = new ArrayList<>();
    private List<Post> banners = new ArrayList<>();

    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private boolean isHeader;
    public String url;
    public String ad;

    public void insertBannerData(List<Post> posts) {
        this.banners = posts;
        notifyItemChanged(0);
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Post obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterPostList(Context context, RecyclerView view, List<Post> items, boolean isHeader) {
        this.items = items;
        ctx = context;
        lastItemViewDetector(view);
        this.isHeader = isHeader;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        public TabLayout tabLayout;
        public ViewPager viewPager;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            tabLayout = (TabLayout) itemView.findViewById(R.id.tab_layout);
            viewPager = (ViewPager) itemView.findViewById(R.id.pager);
        }
    }



    class AdViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public AdViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.ad);
        }
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView short_content;
        public TextView date;
        public TextView comment;
        public ImageView image;
        public LinearLayout lyt_parent;

        public OriginalViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            short_content = (TextView) v.findViewById(R.id.short_content);
            date = (TextView) v.findViewById(R.id.date);
            comment = (TextView) v.findViewById(R.id.comment);
            image = (ImageView) v.findViewById(R.id.image);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            vh = new HeaderViewHolder(v);
        } else if (viewType == VIEW_AD) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad, parent, false);
            vh = new AdViewHolder(v);
        } else if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final Post p = items.get(getCount(position));
            OriginalViewHolder vItem = (OriginalViewHolder) holder;
            vItem.title.setText(Html.fromHtml(p.title));
            vItem.short_content.setText(Html.fromHtml(p.excerpt));
            vItem.date.setText(Tools.getFormatedDateSimple(p.date));
            vItem.comment.setText(p.comment_count+"");


            Author a = p.author;

            if (a.id == 527) {
                Tools.displayImageThumbnail(ctx, "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png", vItem.image);
            }else if (a.id == 2) {
                Tools.displayImageThumbnail(ctx, "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png", vItem.image);
            }else if (a.id == 6) {
                Tools.displayImageThumbnail(ctx, "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png", vItem.image);
            }else if (a.id == 818) {
                Tools.displayImageThumbnail(ctx,"http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png", vItem.image);
            }else{
                Tools.displayImageThumbnail(ctx, p, vItem.image);
            }


            Log.d("xxx", Html.fromHtml(p.title) + " " + Tools.getPostThumbnailUrl(p) + " " + p.thumbnail + " " );
            if(p.thumbnail_images != null && p.thumbnail_images.full != null){
                Log.d("url",p.thumbnail_images.full.url);
            }

            if(p.thumbnail_images != null && p.thumbnail_images.large != null){
                Log.d("url",p.thumbnail_images.large.url);
            }

            if(p.thumbnail_images != null && p.thumbnail_images.medium_large != null){
                Log.d("url",p.thumbnail_images.medium_large.url);
            }

            if(p.thumbnail_images != null && p.thumbnail_images.medium != null){
                Log.d("url",p.thumbnail_images.medium.url);
            }



            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, p, position);
                    }
                }
            });
        }else if (holder instanceof AdViewHolder) {

            AdViewHolder vItem = (AdViewHolder) holder;
            Tools.displayImageThumbnail(ctx,url,vItem.imageView);

            vItem.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tools.directLinkToBrowser((AppCompatActivity) ctx,ad);
                }
            });
        }else if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder hItem = (HeaderViewHolder) holder;


            hItem.tabLayout.removeAllTabs();
            List<Post> bannerPosts = new ArrayList<>();
            for(Post post:banners){

                bannerPosts.add(post);
                hItem.tabLayout.addTab(hItem.tabLayout.newTab().setText(String.valueOf(hItem.tabLayout.getTabCount() + 1)));
                if(bannerPosts.size() == 10){
                    break;
                }
            }
            //hItem.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            AdapterSlider adapter = new AdapterSlider(((AppCompatActivity) ctx).getSupportFragmentManager(), bannerPosts, hItem.tabLayout.getTabCount());
            hItem.viewPager.setAdapter(adapter);
            hItem.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(hItem.tabLayout));
            hItem.tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    hItem.viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        } else{
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (isHeader()){
            return items.size() + 2;
        }else{
            return items.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(isPositionAd(position)){
            return VIEW_AD;
        }else if(isPositionHeader(position)){
            return VIEW_HEADER;
        }else{
            return this.items.get(getCount(position)) != null ? VIEW_ITEM : VIEW_PROG;
        }
    }

    private boolean isPositionAd(int position)
    {
        return position == 1 && isHeader();
    }

    private boolean isPositionHeader(int position)
    {
        return position == 0 && isHeader();
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void insertData(List<Post> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setLoaded() {
        loading = false;
        for(int i = 0; i < getCount(getItemCount()) ; i++){
            if(items.get(i) == null){
                items.remove(i);
                notifyItemRemoved(i + 2);
            }
        }
    }

    public void setLoading() {
        if (getCount(getItemCount()) != 0) {
            this.items.add(null);
            notifyItemInserted(getCount(getItemCount()));
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private int getCount(int count){
        if(isHeader()){
            return count - 2;
        }else{
            return count;
        }
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / Constant.POST_PER_REQUEST;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

}