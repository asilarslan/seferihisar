package com.app.koran.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.koran.fragment.FragmentSlider;
import com.app.koran.model.Post;

import java.util.List;

public class AdapterSlider  extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    List<Post> mPosts;

    public AdapterSlider(FragmentManager fm, List<Post> posts, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mPosts = posts;
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentSlider.newInstance(mPosts.get(position));
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
