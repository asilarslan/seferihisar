package com.app.koran.services;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.app.koran.interfaces.IHeaderSliderResult;
import com.app.koran.model.Attachment;
import com.app.koran.model.Author;
import com.app.koran.model.Category;
import com.app.koran.model.Comment;
import com.app.koran.model.Image;
import com.app.koran.model.Images;
import com.app.koran.model.Post;
import com.app.koran.model.Thumbnails;
import com.app.koran.utils.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HeadeSliderService extends AsyncTask<Void, Void, Void> {


    private String TAG = HeadeSliderService.class.getSimpleName();
    //    private static String url = Constants.URL_CEKILIS_TARIHLERI_PIYANGO;
    private Activity activity;
    private IHeaderSliderResult headerSliderResult;
    private String urlCekilisTarihleri;
    //private ProgressDialog pDialog;
    private List<Post> postList = new ArrayList<>();

    public HeadeSliderService(Activity activity, IHeaderSliderResult headerSliderResult, String urlCekilisTarihleri) {
        this.activity = activity;
        this.headerSliderResult = headerSliderResult;
        this.urlCekilisTarihleri = urlCekilisTarihleri;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Showing progress dialog
        /*pDialog = new ProgressDialog(activity);
        pDialog.setMessage(activity.getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();*/

    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler();

        // Making a request to url and getting response
        String jsonStr = sh.makeServiceCall(urlCekilisTarihleri);

        Log.e(TAG, "Response from url: " + jsonStr);

        if (jsonStr != null) {
            try {

                JSONObject obj = new JSONObject(jsonStr);

                JSONArray jsonArray = obj.getJSONArray("posts");



                for(int i = 0; i<jsonArray.length() ; i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    Post post = new Post();
                    if(jsonObject.has("id"))
                    post.id = jsonObject.getInt("id");
                    if(jsonObject.has("type"))
                    post.type = jsonObject.getString("type");
                    if(jsonObject.has("slug"))
                    post.slug = jsonObject.getString("slug");
                    if(jsonObject.has("url"))
                    post.url = jsonObject.getString("url");
                    if(jsonObject.has("status"))
                    post.status = jsonObject.getString("status");
                    if(jsonObject.has("title"))
                    post.title = jsonObject.getString("title");
                    if(jsonObject.has("title_plain"))
                    post.title_plain = jsonObject.getString("title_plain");
                    if(jsonObject.has("content"))
                    post.content = jsonObject.getString("content");
                    if(jsonObject.has("date"))
                    post.date = jsonObject.getString("date");
                    if(jsonObject.has("modified"))
                    post.modified = jsonObject.getString("modified");
                    if(jsonObject.has("thumbnail"))
                        post.thumbnail = jsonObject.getString("thumbnail");
                    if(jsonObject.has("comment_count"))
                        post.comment_count = jsonObject.getLong("comment_count");

                    if(jsonObject.has("categories")){
                        JSONArray jsonCategoriesArray = jsonObject.getJSONArray("categories");

                        for(int ci = 0; ci<jsonCategoriesArray.length() ; ci++) {
                            JSONObject jsonCategoryObject = jsonCategoriesArray.getJSONObject(ci);

                            Category category = new Category();
                            category.id = jsonCategoryObject.getInt("id");
                            category.slug = jsonCategoryObject.getString("slug");
                            category.title = jsonCategoryObject.getString("title");
                            category.description = jsonCategoryObject.getString("description");
                            category.parent = jsonCategoryObject.getInt("parent");
                            category.post_count = jsonCategoryObject.getInt("post_count");
                            post.categories.add(category);
                        }
                    }

                    if(jsonObject.has("author")){
                        JSONObject jsonObjectAuthor = jsonObject.getJSONObject("author");
                        Author author = new Author();
                        author.id = jsonObjectAuthor.getInt("id");
                        author.slug = jsonObjectAuthor.getString("slug");
                        author.name = jsonObjectAuthor.getString("name");
                        author.first_name = jsonObjectAuthor.getString("first_name");
                        author.last_name = jsonObjectAuthor.getString("last_name");
                        author.nickname = jsonObjectAuthor.getString("nickname");
                        author.url = jsonObjectAuthor.getString("url");
                        author.description = jsonObjectAuthor.getString("description");

                        post.author = author;
                    }

                    if(jsonObject.has("attachments")){
                        JSONArray jsonAttachmentsArray = jsonObject.getJSONArray("attachments");

                        for(int ai = 0; ai<jsonAttachmentsArray.length() ; ai++) {

                            JSONObject jsonAttachmentObject = jsonAttachmentsArray.getJSONObject(ai);

                            Attachment attachment = new Attachment();
                            attachment.id = jsonAttachmentObject.getInt("id");
                            attachment.url = jsonAttachmentObject.getString("url");
                            attachment.mime_type = jsonAttachmentObject.getString("mime_type");

                            if(jsonAttachmentObject.has("images")){
                                Images images = new Images();
                                Image imageFull = new Image();
                                imageFull.url = jsonAttachmentObject.getJSONObject("images").getJSONObject("full").getString("url");
                                images.full = imageFull;
                                Image imageLarge = new Image();
                                imageLarge.url = jsonAttachmentObject.getJSONObject("images").getJSONObject("large").getString("url");
                                images.large = imageLarge;
                                Image imageMediumLarge = new Image();
                                imageMediumLarge.url = jsonAttachmentObject.getJSONObject("images").getJSONObject("medium_large").getString("url");
                                images.medium_large = imageMediumLarge;
                                Image imageMedium = new Image();
                                imageMedium.url = jsonAttachmentObject.getJSONObject("images").getJSONObject("medium").getString("url");
                                images.medium = imageMedium;
                                attachment.images = images;
                            }

                            post.attachments.add(attachment);


                        }
                    }


                    if(jsonObject.has("thumbnail_images")){
                        Thumbnails thumbnails = new Thumbnails();
                        JSONObject jothumbnails = jsonObject.getJSONObject("thumbnail_images");
                        Image fullImage = new Image();
                        fullImage.url = jothumbnails.getJSONObject("full").getString("url");
                        thumbnails.full = fullImage;

                        Image largeImage = new Image();
                        largeImage.url = jothumbnails.getJSONObject("large").getString("url");
                        thumbnails.large = largeImage;

                        Image mediumImage = new Image();
                        mediumImage.url = jothumbnails.getJSONObject("medium").getString("url");
                        thumbnails.medium = mediumImage;

                        Image mediumLargeImage = new Image();
                        mediumLargeImage.url = jothumbnails.getJSONObject("medium_large").getString("url");
                        thumbnails.medium_large = mediumLargeImage;

                        post.thumbnail_images = thumbnails;
                    }


                    if(jsonObject.has("comments")){
                        JSONArray jsonCommentsArray = jsonObject.getJSONArray("comments");

                        for(int ci = 0; ci<jsonCommentsArray.length() ; ci++) {

                            JSONObject jsonCommentObject = jsonCommentsArray.getJSONObject(ci);

                            Comment comment = new Comment();
                            comment.id = jsonCommentObject.getLong("id");
                            comment.name = jsonCommentObject.getString("name");
                            comment.url = jsonCommentObject.getString("url");
                            comment.date = jsonCommentObject.getString("date");
                            comment.content = jsonCommentObject.getString("content");
                            comment.parent = jsonCommentObject.getInt("parent");

                            post.comments.add(comment);

                        }

                    }


                    if(jsonObject.getJSONObject("custom_fields").has("swp_primaryshow")){
                        postList.add(post);
                    }


                }


            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());

            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        // Dismiss the progress dialog
        /*if (pDialog.isShowing())
            pDialog.dismiss();*/
        headerSliderResult.resultHandle(postList);
    }

}