package com.app.koran;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.koran.adapter.ImageAdapter;
import com.app.koran.connection.API;
import com.app.koran.connection.RestAdapter;
import com.app.koran.connection.callbacks.CallbackDetailsPost;
import com.app.koran.data.AppConfig;
import com.app.koran.data.Constant;
import com.app.koran.data.SharedPref;
import com.app.koran.model.Attachment;
import com.app.koran.model.Author;
import com.app.koran.model.Post;
import com.app.koran.realm.RealmController;
import com.app.koran.utils.NetworkCheck;
import com.app.koran.utils.Tools;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityGallery extends AppCompatActivity {

    public static final String EXTRA_OBJC = "key.EXTRA_OBJC";
    public static final String EXTRA_NOTIF = "key.EXTRA_NOTIF";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionView, Post obj) {
        Intent intent = new Intent(activity, ActivityGallery.class);
        intent.putExtra(EXTRA_OBJC, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionView, EXTRA_OBJC);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private View lyt_parent;
    private View lyt_image_header;
    private MenuItem read_later_menu;
    private SwipeRefreshLayout swipe_refresh;

    // extra obj
    private Post post;
    private boolean from_notif;

    private SharedPref sharedPref;
    private boolean flag_read_later;
    private Call<CallbackDetailsPost> callbackCall = null;

    // for fullscreen video
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private CustomWebChromeClient customWebChromeClient;


    private GridView imageGrid;
    private ArrayList<Bitmap> bitmapList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // get extra object
        post = (Post) getIntent().getSerializableExtra(EXTRA_OBJC);

       initToolbar();

        ((TextView) findViewById(R.id.title)).setText(Html.fromHtml(post.title));

        this.imageGrid = (GridView) findViewById(R.id.gridview);
        this.bitmapList = new ArrayList<Bitmap>();

        List<Attachment> attachments = post.attachments;

        this.imageGrid.setAdapter(new ImageAdapter(this, this.bitmapList, attachments));


        try {
            for(Attachment attachment:attachments){
                this.bitmapList.add(urlImageToBitmap(attachment.images.medium.url));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageAdapter imageAdapter = new ImageAdapter(this, this.bitmapList,attachments);

        this.imageGrid.setAdapter(imageAdapter);




    }

    private Bitmap urlImageToBitmap(String imageUrl) throws Exception {
        Bitmap result = null;
        URL url = new URL(imageUrl);
        if(url != null) {
            //BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inPreferredConfig = Bitmap.Config.RGB_565;
            final BitmapFactory.Options options = new BitmapFactory.Options();


            result = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            //result = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
        }
        return result;
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
    }

    private void requestDetailsPostApi() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getPostDetailsById(post.id);
        callbackCall.enqueue(new Callback<CallbackDetailsPost>() {
            @Override
            public void onResponse(Call<CallbackDetailsPost> call, Response<CallbackDetailsPost> response) {
                CallbackDetailsPost resp = response.body();
                if (resp != null && resp.status.equals("ok")) {
                    post = resp.post;
                    displayPostData(false);
                    swipeProgress(false);
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackDetailsPost> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void requestAction() {
        showFailedView(false, "");
        swipeProgress(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestDetailsPostApi();
            }
        }, Constant.DELAY_TIME_MEDIUM);
    }

    private void onFailRequest() {
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private WebView webview;

    private void displayPostData(boolean is_draft) {
        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        ((TextView) findViewById(R.id.title)).setText(Html.fromHtml(post.title));

        String html_data = "<style>img{max-width:100%;height:auto;} figure{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += post.content;
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings();
        webview.getSettings().setBuiltInZoomControls(true);
        webview.setBackgroundColor(Color.TRANSPARENT);
        customWebChromeClient = new CustomWebChromeClient();
        webview.setWebChromeClient(customWebChromeClient);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Tools.isUrlFromImageType(url)) {
                    ActivityFullScreenImage.navigate(ActivityGallery.this, url);
                } else {
                    Tools.directLinkToBrowser(ActivityGallery.this, url);
                }
                return true;
            }
        });
        webview.loadData(html_data, "text/html; charset=UTF-8", null);
        // disable scroll on touch
        webview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

        ((TextView) findViewById(R.id.date)).setText(Tools.getFormatedDate(post.date));
        ((TextView) findViewById(R.id.comment)).setText(post.comment_count + "");
        ((TextView) findViewById(R.id.tv_comment)).setText(getString(R.string.show_tv_comments) + " (" + post.comment_count + ")");
        ((TextView) findViewById(R.id.category)).setText(Html.fromHtml(Tools.getCategoryTxt(post.categories)));
        Tools.displayImageThumbnail(this, post, ((ImageView) findViewById(R.id.image)));




        final Author a = post.author;

        ((Button) findViewById(R.id.avatarButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityAuthorPostDetails.navigate(ActivityGallery.this, findViewById(R.id.avatarView) ,a );

            }
        });

        ((TextView) findViewById(R.id.namesurname)).setText(a.first_name+ " " + a.last_name);
        if (a.id == 527) {
            findViewById(R.id.avatarView).setVisibility(View.VISIBLE);
            findViewById(R.id.imageView).setVisibility(View.GONE);
            Tools.displayImageThumbnail(this, "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png", ((ImageView) findViewById(R.id.avatar)));
        }else if (a.id == 2) {
            findViewById(R.id.avatarView).setVisibility(View.VISIBLE);
            findViewById(R.id.imageView).setVisibility(View.GONE);
            Tools.displayImageThumbnail(this, "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png", ((ImageView) findViewById(R.id.avatar)));
        }else if (a.id == 6) {
            findViewById(R.id.avatarView).setVisibility(View.VISIBLE);
            findViewById(R.id.imageView).setVisibility(View.GONE);
            Tools.displayImageThumbnail(this, "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png", ((ImageView) findViewById(R.id.avatar)));
        }else if (a.id == 818) {
            findViewById(R.id.avatarView).setVisibility(View.VISIBLE);
            findViewById(R.id.imageView).setVisibility(View.GONE);
            Tools.displayImageThumbnail(this,"http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png", ((ImageView) findViewById(R.id.avatar)));
        }else{
            findViewById(R.id.avatarView).setVisibility(View.GONE);
            findViewById(R.id.imageView).setVisibility(View.VISIBLE);
        }

        if (is_draft) {
            return;
        }
        // when show comments click


        // when post comments click
        ((MaterialRippleLayout) findViewById(R.id.bt_send_comment)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!AppConfig.MUST_REGISTER_TO_COMMENT) {
                    Intent i = new Intent(ActivityGallery.this, ActivityWebView.class);
                    if (sharedPref.isRespondEnabled()) {
                        i = new Intent(ActivityGallery.this, ActivitySendComment.class);
                    }
                    i.putExtra(EXTRA_OBJC, post);
                    startActivity(i);
                } else {
                    Tools.dialogCommentNeedLogin(ActivityGallery.this, post.url);
                }
            }
        });

        lyt_image_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String thumb = Tools.getPostThumbnailUrl(post);
                if (Tools.isUrlFromImageType(thumb)) {
                    ActivityFullScreenImage.navigate(ActivityGallery.this, thumb);
                }
            }
        });

        //Snackbar.make(parent_view, R.string.post_detail_displayed_msg, Snackbar.LENGTH_SHORT).show();
    }



    private void refreshReadLaterMenu() {
        flag_read_later = RealmController.with(this).getPost(post.id) != null;
        Drawable drawable = read_later_menu.getIcon();
        if (flag_read_later) {
            drawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        } else {
            drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void prepareAds() {
        if (AppConfig.ENABLE_ADSENSE && NetworkCheck.isConnect(getApplicationContext())) {
            AdView mAdView = (AdView) findViewById(R.id.ad_view);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
        } else {
            ((RelativeLayout) findViewById(R.id.banner_layout)).setVisibility(View.GONE);
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        View lyt_main_content = (View) findViewById(R.id.lyt_main_content);

        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_main_content.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_main_content.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (from_notif) {
            startActivity(new Intent(getApplicationContext(), ActivityMain.class));
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        //webview.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        //webview.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //if (mCustomView != null) {
          //  customWebChromeClient.onHideCustomView();
        //}
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mCustomView != null) {
               // customWebChromeClient.onHideCustomView();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class CustomWebChromeClient extends WebChromeClient {
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            // landscape and fullscreen
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            Tools.toggleFullScreenActivity(ActivityGallery.this, true);

            mCustomView = view;
            lyt_parent.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {
            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(ActivityGallery.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            if (mCustomView == null) return;

            // revert landscape and fullscreen
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            Tools.toggleFullScreenActivity(ActivityGallery.this, false);

            lyt_parent.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

}