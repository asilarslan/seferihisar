package com.app.koran.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.koran.ActivityAuthorPostDetails;
import com.app.koran.R;
import com.app.koran.connection.API;
import com.app.koran.connection.RestAdapter;
import com.app.koran.connection.callbacks.CallbackListPost;
import com.app.koran.model.Author;
import com.app.koran.model.Category;
import com.app.koran.model.Post;
import com.app.koran.utils.Tools;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterAuthor extends RecyclerView.Adapter<AdapterAuthor.ViewHolder> {

    private List<Author> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Author author, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterAuthor(Context context, List<Author> items) {
        this.items = items;
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView authorAvatar;
        public TextView authorNameSurname;
        public TextView authorPostTitle;
        public TextView date;
        public LinearLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            authorAvatar = (ImageView) v.findViewById(R.id.postImage);
            authorNameSurname = (TextView) v.findViewById(R.id.authorNameSurname);
            authorPostTitle = (TextView) v.findViewById(R.id.postTitle);
            date = (TextView) v.findViewById(R.id.date);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_author, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Author a = items.get(position);

        holder.authorNameSurname.setText((Html.fromHtml(a.first_name + " " + a.last_name)));
        if (a.id == 527) {
            Tools.displayImageThumbnail(ctx, "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png", holder.authorAvatar);
        }else if (a.id == 2) {
            Tools.displayImageThumbnail(ctx, "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png", holder.authorAvatar);
        }else if (a.id == 6) {
            Tools.displayImageThumbnail(ctx, "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png", holder.authorAvatar);
        }else if (a.id == 818) {
            Tools.displayImageThumbnail(ctx,"http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png", holder.authorAvatar);
        }


        API api = RestAdapter.createAPI();
        Call<CallbackListPost> callbackCall  = api.getAuthorPost(a.id,1);
        callbackCall.enqueue(new Callback<CallbackListPost>() {
            @Override
            public void onResponse(Call<CallbackListPost> call, Response<CallbackListPost> response) {
                CallbackListPost resp = response.body();
                if (resp != null && resp.status.equals("ok")) {
                    List<Post> posts = resp.posts;
                    if(posts.size() > 0 ){
                        Post post = posts.get(0);
                        holder.authorPostTitle.setText(post.title);
                        holder.date.setText(Tools.getFormatedDateSimple(post.date));
                    }
                } else {
                    //onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<CallbackListPost> call, Throwable t) {
                // if (!call.isCanceled()) onFailRequest(page_no);
            }

        });


       /* holder.name.setText(Html.fromHtml(c.title));
        holder.post_count.setText(c.post_count+"");
*/
        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, a, position);
                }
            }
        });
    }

    public void setListData(List<Author> items){
        this.items = items;
        notifyDataSetChanged();
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }



}