package com.app.koran.fragment;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.koran.ActivityMain;
import com.app.koran.ActivityPageDetails;
import com.app.koran.ActivityPostDetails;
import com.app.koran.ActivitySearch;
import com.app.koran.R;
import com.app.koran.model.Post;
import com.app.koran.utils.Tools;

public class FragmentSlider extends Fragment {


    public TextView title;
    public ImageView image;

    public static FragmentSlider newInstance(Post post) {
        FragmentSlider fragmentSlider = new FragmentSlider();

        Bundle args = new Bundle();
        args.putSerializable("post", post);
        fragmentSlider.setArguments(args);

        return fragmentSlider;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slider, container, false);
        title = (TextView) view.findViewById(R.id.title);
        image = (ImageView) view.findViewById(R.id.image);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Post post = (Post) getArguments().getSerializable("post");
        if(post != null){
            title.setText(Html.fromHtml(post.title));
            Tools.displayImageBanner(getContext(), post, image);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityPostDetails.navigate((ActivityMain) getActivity(), v.findViewById(R.id.image), post);
                }
            });
        }
    }
}