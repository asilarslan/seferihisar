package com.app.koran.model;

import com.app.koran.realm.table.AttachmentRealm;
import com.app.koran.realm.table.ImagesRealm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Attachment implements Serializable {
    public long id = -1;
    public String url;
    public String mime_type;
    public Images images;

    public AttachmentRealm getObjectRealm() {
        AttachmentRealm a = new AttachmentRealm();
        a.id = id;
        a.url = url;
        a.mime_type = mime_type;
        a.images = images.getObjectRealm();
        return a;
    }
}
