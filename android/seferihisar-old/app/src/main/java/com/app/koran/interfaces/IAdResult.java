package com.app.koran.interfaces;

import com.app.koran.model.Post;

import java.util.List;

public interface IAdResult {
    public void resultHandle(String url, String ad);
}
