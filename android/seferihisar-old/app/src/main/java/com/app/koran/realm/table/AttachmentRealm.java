package com.app.koran.realm.table;

import com.app.koran.model.Attachment;
import com.app.koran.model.Images;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AttachmentRealm extends RealmObject {

    @PrimaryKey
    public long id = -1;
    public String url;
    public String mime_type;
    public ImagesRealm images;

    public Attachment getOriginal() {
        Attachment a = new Attachment();
        a.id = id;
        a.url = url;
        a.mime_type = mime_type;
        a.images = images.getOriginal();
        return a;
    }

}
