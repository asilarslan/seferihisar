package com.app.koran.services;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.app.koran.interfaces.IAdResult;
import com.app.koran.interfaces.IHeaderSliderResult;
import com.app.koran.model.Attachment;
import com.app.koran.model.Author;
import com.app.koran.model.Category;
import com.app.koran.model.Comment;
import com.app.koran.model.Image;
import com.app.koran.model.Post;
import com.app.koran.model.Thumbnails;
import com.app.koran.utils.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdService extends AsyncTask<Void, Void, Void> {


    private String TAG = AdService.class.getSimpleName();
    //    private static String url = Constants.URL_CEKILIS_TARIHLERI_PIYANGO;
    private Activity activity;
    private IAdResult headerSliderResult;
    private String urlCekilisTarihleri;
    //private ProgressDialog pDialog;
    private String url = "";
    private String ad = "";

    public AdService(Activity activity, IAdResult headerSliderResult, String urlCekilisTarihleri) {
        this.activity = activity;
        this.headerSliderResult = headerSliderResult;
        this.urlCekilisTarihleri = urlCekilisTarihleri;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Showing progress dialog
        /*pDialog = new ProgressDialog(activity);
        pDialog.setMessage(activity.getString(R.string.please_wait));
        pDialog.setCancelable(false);
        pDialog.show();*/

    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler();

        // Making a request to url and getting response
        String jsonStr = sh.makeServiceCall(urlCekilisTarihleri);

        Log.e(TAG, "Response from url: " + jsonStr);

        if (jsonStr != null) {
            try {

                JSONObject obj = new JSONObject(jsonStr);

                url = obj.getString("photoUrl");
                ad = obj.getString("adUrl");



            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());

            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        // Dismiss the progress dialog
        /*if (pDialog.isShowing())
            pDialog.dismiss();*/
        headerSliderResult.resultHandle(url,ad);
    }

}