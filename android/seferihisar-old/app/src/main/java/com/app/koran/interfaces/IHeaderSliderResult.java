package com.app.koran.interfaces;

import com.app.koran.model.Post;

import java.util.List;

public interface IHeaderSliderResult {
    public void resultHandle(List<Post> postList);
}
