package com.app.koran.model;

import com.app.koran.realm.table.ImageRealm;
import com.app.koran.realm.table.ImagesRealm;

import java.io.Serializable;

public class Images  implements Serializable {

    public Image full;
    public Image medium;
    public Image medium_large;
    public Image large;

    public ImagesRealm getObjectRealm() {
        ImagesRealm t = new ImagesRealm();
        t.full = (full != null ? full.getObjectRealm() : null);
        t.medium = (medium != null ? medium.getObjectRealm() : null);
        t.medium_large = (medium_large != null ? medium_large.getObjectRealm() : null);
        t.large = (large != null ? large.getObjectRealm() : null);
        return t;
    }

}