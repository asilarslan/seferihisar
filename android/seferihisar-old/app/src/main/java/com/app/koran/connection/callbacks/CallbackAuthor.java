package com.app.koran.connection.callbacks;

import com.app.koran.model.Author;

import java.util.ArrayList;
import java.util.List;

public class CallbackAuthor {

    public String status = "";
    public int count = -1;
    public List<Author> authors = new ArrayList<>();
}
