package com.app.koran.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.koran.ActivityAuthorPostDetails;
import com.app.koran.ActivityMain;
import com.app.koran.ActivityPostDetails;
import com.app.koran.R;
import com.app.koran.adapter.AdapterAuthor;
import com.app.koran.adapter.AdapterPostList;
import com.app.koran.connection.API;
import com.app.koran.connection.RestAdapter;
import com.app.koran.connection.callbacks.CallbackAd;
import com.app.koran.connection.callbacks.CallbackAuthor;
import com.app.koran.connection.callbacks.CallbackListPost;
import com.app.koran.data.Constant;
import com.app.koran.model.Author;
import com.app.koran.model.Category;
import com.app.koran.model.Post;
import com.app.koran.utils.NetworkCheck;
import com.app.koran.utils.Tools;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAuthor extends Fragment {

    private View root_view, parent_view;
    private RecyclerView recyclerView;
    private AdapterAuthor mAdapter;
    private SwipeRefreshLayout swipe_refresh;
    private Call<CallbackAuthor> callbackCall = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_author, null);
        parent_view = getActivity().findViewById(R.id.main_content);

        swipe_refresh = (SwipeRefreshLayout) root_view.findViewById(R.id.swipe_refresh_layout_home);
        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerViewHome);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterAuthor(getContext(),new ArrayList<Author>());
        recyclerView.setAdapter(mAdapter);

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction();
            }
        });

        mAdapter.setOnItemClickListener(new AdapterAuthor.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Author author, int position) {

                //ActivityPostDetails.navigate((ActivityMain) getActivity(), v.findViewById(R.id.image), obj);
                ActivityAuthorPostDetails.navigate((ActivityMain) getActivity(),view.findViewById(R.id.lyt_parent),author);
            }
        });

        requestAction();

        // get enabled controllers
        Tools.requestInfoApi(getActivity());

        return root_view;
    }

    private void displayApiResult(final List<Author> posts) {
        List<Author> tempPosts = new ArrayList<>();
        for(Author author:posts) {
            if (author.id == 527 || author.id == 2 || author.id == 6 || author.id == 818) {
                tempPosts.add(author);
            }
        }
        mAdapter.setListData(tempPosts);
        swipeProgress(false);
        if (posts.size() == 0) {
            showNoItemView(true);
        }
    }


    private void requestListPostApi() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getAuthors();
        callbackCall.enqueue(new Callback<CallbackAuthor>() {
            @Override
            public void onResponse(Call<CallbackAuthor> call, Response<CallbackAuthor> response) {
                CallbackAuthor resp = response.body();
                if (resp != null && resp.status.equals("ok")) {
                    displayApiResult(resp.authors);
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackAuthor> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }





    private void onFailRequest() {
        //mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction() {
        showFailedView(false, "");
        showNoItemView(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListPostApi();
            }
        }, Constant.DELAY_TIME);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) root_view.findViewById(R.id.lyt_failed_home);
        ((TextView) root_view.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) root_view.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) root_view.findViewById(R.id.lyt_no_item_home);
        ((TextView) root_view.findViewById(R.id.no_item_message)).setText(R.string.no_post);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

}

