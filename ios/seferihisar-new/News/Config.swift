//
//  Config.swift
//  News
//
//  Created by Asil Arslan on 21.12.2020.
//

import Foundation
import SwiftUI

//Your wordpress address
var URL_HOST = "https://seferihisar.com"

var URL_HEADLINE_POSTS = "\(URL_HOST)/wp-json/wp/v2/posts?status=publish&categories=2,6" // &author=1
var URL_AUTHOR_POSTS = "\(URL_HOST)/wp-json/wp/v2/posts?status=publish&author="
var URL_POSTS = "\(URL_HOST)/wp-json/wp/v2/posts?status=publish" // &author=1
var URL_PAGES = "\(URL_HOST)/wp-json/wp/v2/pages?status=publish"
var URL_COMMENTS = "\(URL_HOST)/wp-json/wp/v2/comments"
var URL_POST_COMMENTS = "\(URL_HOST)/wp-json/wp/v2/comments?status=approve&post="
var URL_CATEGORIES = "\(URL_HOST)/wp-json/wp/v2/categories"
var URL_CATEGORY_POST = "\(URL_HOST)/wp-json/wp/v2/posts?status=publish&categories="
var URL_TAGS = "\(URL_HOST)/wp-json/wp/v2/tags?per_page=100"
var URL_TAG_POST = "\(URL_HOST)/wp-json/wp/v2/posts?status=publish"

///Onboard Data
var ONBOARD_DATA: [Onboard] = [
    Onboard(title: "Yeni Haber Gazetesi", headline: "Seferihisar ve çevresinde 40 yıldır yayın yapan Yeni Haber Gazetesi'nin mobil uygulaması.", image: "logo", gradientColors: [Color("ColorBlue"), Color("ColorBlue")]),
    Onboard(title: "Yeni Haber Gazetesi", headline: "Yeni Haberleri oku", image: "logo", gradientColors: [Color("ColorGreen"), Color("ColorGreen")]),
    Onboard(title: "Yeni Haber Gazetesi", headline: "Yorum Yap", image: "logo", gradientColors: [Color("ColorRed"), Color("ColorRed")])
]

///In App Products
var IN_APP_PRODUCTS = [
    //Use your product IDs instead
    "remove_ads",
    "donate"
]

///Category Tabs Visibilty
var IS_CATEGORIES_VISIBLE = true

///Headline Visibilty
var IS_HEADLINE_VISIBLE = true

///Headline Type
var HEADLINE_TYPE = HeadlineEnum.multiple
var EMPTY_IMAGE_URL = "https://seferihisar.com/wp-content/themes/shnews/assets/img/no-thumb.jpg"

//Admob Interstital ad id
var INTERSTITIAL_AD_ID: String = "ca-app-pub-8915706349520340/3989286675"
var TEST_DEVICE: String = "b317b731f5490205b1f60b638837ea16"

enum HeadlineEnum {
    case single, multiple
}

//var URL_HOST = "https://time.com/"
//var URL_HEADLINE_POSTS = "\(URL_HOST)/wp-json/wp/v2/posts?_embed&status=publish&categories=1258"
