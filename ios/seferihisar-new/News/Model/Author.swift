//
//  Author.swift
//  Seferihisar
//
//  Created by Asil Arslan on 28.12.2020.
//

import SwiftUI

struct Author: Codable, Identifiable {
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case photo = "photo"
        case first_name = "first_name"
        case last_name = "last_name"
        case post_title = "post_title"
    }
    
    let id: Int
    let photo: String?
    let first_name: String
    let last_name: String
    let post_title: String
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try! values.decode(Int.self, forKey: .id)
        photo = try? values.decode(String.self, forKey: .photo)
        first_name = try! values.decode(String.self, forKey: .first_name)
        last_name = try! values.decode(String.self, forKey: .last_name)
        post_title = try! values.decode(String.self, forKey: .post_title)
    }
    
    init(id: Int, photo: String, first_name: String, last_name: String, post_title: String) {
        self.id = id
        self.photo = photo
        self.first_name = first_name
        self.last_name = last_name
        self.post_title = post_title
    }
    
    static var `default` : Author {
        Author(id: 0, photo: EMPTY_IMAGE_URL, first_name: "Author", last_name: "Author", post_title: "Author")
    }
    
}

//struct AuthorPost: Codable, Identifiable {
//
//    enum CodingKeys: String, CodingKey {
//        case id = "id"
//        case post_title = "post_title"
//        case post_date = "post_date"
//    }
//
//    let id: Int
//    let post_title: String
//    let post_date: String
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try! values.decode(Int.self, forKey: .id)
//        post_title = try values.decode(String.self, forKey: .post_title)
//        post_date = try! values.decode(String.self, forKey: .post_date)
//    }
//
//}
