//
//  AuthorPost.swift
//  Seferihisar
//
//  Created by Asil Arslan on 31.05.2021.
//

import Foundation
import SwiftUI

struct AuthorPost: Codable, Identifiable {
    let id: Int
    let title: String
    let excerpt: String
    let content: String
    let date: String
    let url: String
    
    static var `default` : AuthorPost {
        AuthorPost(id: 0, title: "Title", excerpt: "Excerpt", content: "Content", date: "2020-12-18 16:45:09", url: "")
    }
    
    //    required init(from decoder: Decoder) throws {
    //            let values = try decoder.container(keyedBy: CodingKeys.self)
    //        id = try values.decode(Int.self, forKey: .id)
    //        date = try values.decode(String.self, forKey: .date)
    //        title = try values.decode(Rendered.self, forKey: .title)
    //        content = try values.decode(Rendered.self, forKey: .content)
    //        excerpt = try values.decode(Rendered.self, forKey: .excerpt)
    //        link = try values.decode(String.self, forKey: .link)
    //        _embedded = try values.decode(Embedded.self, forKey: ._embedded)
    ////            mess = try values.decode([String].self, forKey: .mess)
    ////            data = try? values.decode(LoginUserResponseData.self, forKey: .data)
    //        }
}

struct AuthorPosts: Codable {
    var posts = [AuthorPost]()
    
    static var `default` : AuthorPosts {
        AuthorPosts(posts: [])
    }
}
