//
//  Interstitial.swift
//  News
//
//  Created by Asil Arslan on 22.12.2020.
//

import SwiftUI
import GoogleMobileAds
import UIKit


    
final class Interstitial:NSObject, GADInterstitialDelegate{
    var interstitial:GADInterstitial = GADInterstitial(adUnitID: INTERSTITIAL_AD_ID)
    
    override init() {
        super.init()
        LoadInterstitial()
    }
    
    func LoadInterstitial(){
        let req = GADRequest()
        self.interstitial.load(req)
        self.interstitial.delegate = self
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [TEST_DEVICE];
    }
    
    func showAd(){
        if self.interstitial.isReady{
           let root = UIApplication.shared.windows.first?.rootViewController
           self.interstitial.present(fromRootViewController: root!)
        }
       else{
           print("Not Ready")
       }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.interstitial = GADInterstitial(adUnitID: INTERSTITIAL_AD_ID)
        LoadInterstitial()
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("Interstitial Received")
    }
}


