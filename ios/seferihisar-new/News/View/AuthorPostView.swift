//
//  AuthorPostView.swift
//  Seferihisar
//
//  Created by Asil Arslan on 31.05.2021.
//

import SwiftUI

struct AuthorPostView: View {
    var aurhorPost: AuthorPost
    var body: some View {
        HStack(alignment: .top, spacing: 16.0) {
            
            VStack(alignment: .leading, spacing: 4.0, content: {
                Text(aurhorPost.title.decodingHTMLEntities()).font(.headline).lineLimit(2)
                Text(aurhorPost.excerpt.decodingHTMLEntities()).lineLimit(2).font(.subheadline)
                Text(aurhorPost.date.toDate2())
                    .foregroundColor(.gray)
                    .font(.subheadline)
            })
            
            Spacer()
        }
        .padding(8.0)
    }
}

struct AuthorPostView_Previews: PreviewProvider {
    static var previews: some View {
        AuthorPostView(aurhorPost: AuthorPost.default)
    }
}
