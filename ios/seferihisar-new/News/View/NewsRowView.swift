//
//  NewsRowView.swift
//  News
//
//  Created by Asil Arslan on 21.12.2020.
//

import SwiftUI
import KingfisherSwiftUI

struct NewsRowView: View {
    let post:New
    var body: some View {
        HStack(alignment: .top, spacing: 16.0) {
            //            Image(news.imageName)
            if post.jetpack_featured_media_url != nil && post.jetpack_featured_media_url != "" {
                if let url = URL(string: (post.jetpack_featured_media_url)!) {
                    KFImage(source: .network(url))
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 120, height: 120)
                    .cornerRadius(8.0)
                }
            }
            
            
            VStack(alignment: .leading, spacing: 4.0, content: {
                Text(post.title.rendered.decodingHTMLEntities()).font(.headline).lineLimit(2)
                Text(post.excerpt.rendered.decodingHTMLEntities()).lineLimit(2).font(.subheadline)
                Text(post.date.toDate())
                    .foregroundColor(.gray)
                    .font(.subheadline)
            })
        }
        .padding(8.0)
    }

}

struct NewsRowView_Previews: PreviewProvider {
    static var previews: some View {
        NewsRowView(post: New.default)
    }
}
