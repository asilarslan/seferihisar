//
//  SettingView.swift
//  News
//
//  Created by Asil Arslan on 21.12.2020.
//

import SwiftUI

struct SettingView: View {
    @Environment(\.presentationMode) var presentationMode
    @AppStorage("isOnboarding") var isOnboarding: Bool = false
//    @StateObject var storeManager: StoreManager
    @Binding var showMenu : Bool
    
    var body: some View {
        NavigationView{
            ScrollView(.vertical, showsIndicators: false){
                VStack(spacing: 20){
                    
                    GroupBox(label:SettingsLabelView(labelText: "SEFERİHİSAR YENİ HABER", labelImage: "info.circle")){
                        
                        Divider().padding(.vertical, 4)
                        
                        HStack(alignment:.center, spacing: 10) {
                            Image("logo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 80, height: 80)
                                .cornerRadius(9)
                            Text(LocalizedStringKey("Info Description"))
                                .font(.footnote)
                        }
                    }
                    
//                    GroupBox(label : SettingsLabelView(labelText: "Premium", labelImage: "suit.diamond")){
////                        SimulatorStoreView()
//                        ForEach(storeManager.myProducts, id: \.self) { product in
//                            Divider().padding(.vertical, 4)
//                            HStack {
//                                VStack(alignment: .leading) {
//                                    Text(product.localizedTitle)
//                                        .font(.headline)
//                                    Text(product.localizedDescription)
//                                        .font(.caption2)
//                                }
//                                Spacer()
//                                if UserDefaults.standard.bool(forKey: product.productIdentifier) {
//                                    Text(LocalizedStringKey("Purchased"))
//                                        .foregroundColor(.green)
//                                } else {
//                                    Button(action: {
//                                        storeManager.purchaseProduct(product: product)
//                                    }) {
//                                        Text("Buy for \(product.price) $")
//                                    }
//                                    .foregroundColor(.blue)
//                                }
//                            }.padding(.vertical, 4)
//                        }
//                    }
                    
                    GroupBox(label : SettingsLabelView(labelText: "ÖZELLİŞTİRME", labelImage: "paintbrush")){
                        
                        Divider().padding(.vertical, 4)
                        Text("Dilerseniz, bu kutudaki anahtarı değiştirerek uygulamayı yeniden başlatabilirsiniz. Bu şekilde işe alım sürecini başlatır ve karşılama ekranını tekrar görürsünüz.")
                            .padding(.vertical, 8)
                            .frame(minHeight: 60)
                            .layoutPriority(1)
                            .font(.footnote)
                            .multilineTextAlignment(.leading)
                        
                        Toggle(isOn: $isOnboarding){
                            if isOnboarding {
                                Text("Başlat".uppercased())
                                    .fontWeight(.bold)
                                    .foregroundColor(.green)
                            } else {
                                Text("Başlat".uppercased())
                                    .fontWeight(.bold)
                                    .foregroundColor(.secondary)
                            }
                        }
                        .padding()
                        .background(Color(.tertiarySystemBackground).clipShape(RoundedRectangle(cornerRadius: 8, style: .continuous)))
                    }
                    
                    GroupBox(label: SettingsLabelView(labelText: "UYGULAMA", labelImage: "apps.iphone")){
                        
                        SettingsRowView(name: "Developer",content: "Asil ARSLAN")
                        SettingsRowView(name: "Compability",content: "iOS 14")
                        SettingsRowView(name: "Website", linkLabel: "seferihisar.com", linkDestination: "seferihisar.com")
                        SettingsRowView(name: "Version",content: "4.0.1")
                        
                    }
                    
                }
                .navigationBarTitle(Text(LocalizedStringKey("Setting")), displayMode: .large)
                .toolbar(content: {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action: {
                            withAnimation{

                                self.showMenu.toggle()
                            }
                        }, label: {

                            Image(systemName: self.showMenu ? "xmark" : "line.horizontal.3")
                                .font(.title)
                                .foregroundColor(.black)
                        })
                    }
//                    ToolbarItem(placement: .navigationBarTrailing) {
//                        Button(action: {
//                            storeManager.restoreProducts()
//                        }) {
//                            Text("Restore Purchases ")
//                        }
//                    }
                })
                .padding()
            }//: ScrollView
            
        }//: NavigationView
        // prevent iPad split view
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct SimulatorStoreView: View {
    var body: some View {
        VStack {
        Divider().padding(.vertical, 4)
            HStack {
                VStack(alignment: .leading) {
                    Text("Donate")
                        .font(.headline)
                    Text("Donate App")
                        .font(.caption2)
                }
                Spacer()
                if UserDefaults.standard.bool(forKey: "*ID of IAP Product*") {
                    Text("Purchased")
                        .foregroundColor(.green)
                } else {
                    Button(action: {
                        //Purchase particular IAO product
                    }) {
                        Text("Buy for 5.09 $")
                    }
                    .foregroundColor(.blue)
                }
            }
        Divider().padding(.vertical, 4)
            HStack {
                VStack(alignment: .leading) {
                    Text("Remove Ads")
                        .font(.headline)
                    Text("Remove All Ads")
                        .font(.caption2)
                }
                Spacer()
                if UserDefaults.standard.bool(forKey: "*ID of IAP Product*") {
                    Text("Purchased")
                        .foregroundColor(.green)
                } else {
                    Button(action: {
                        //Purchase particular IAO product
                    }) {
                        Text("Buy for 1.09 $")
                    }
                    .foregroundColor(.blue)
                }
            }
        }.padding(.vertical, 4)
    }
}

struct SettingView_Previews: PreviewProvider {
    static var previews: some View {
//        SettingView(storeManager: StoreManager(), showMenu: .constant(false))
        SettingView(showMenu: .constant(false))

    }
}
