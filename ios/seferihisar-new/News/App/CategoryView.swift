//
//  CategoryView.swift
//  News
//
//  Created by Asil Arslan on 21.12.2020.
//

import SwiftUI

struct CategoryView: View {
    @State var categories = [Category]()
    @Binding var showMenu : Bool
    var body: some View {
        NavigationView {
//            ForEach (categories) { row in
//                NavigationLink(destination: NewsView(post: row)) {
                List(categories) { row in
                    NavigationLink(destination: CategoryNewsView(category: row)) {
                    CategoryRowView(category: row)
                    }
                }
//                }
//            }
                .navigationBarTitle(LocalizedStringKey("Category"))
                .toolbar(content: {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action: {
                            withAnimation{

                                self.showMenu.toggle()
                            }
                        }, label: {

                            Image(systemName: self.showMenu ? "xmark" : "line.horizontal.3")
                                .font(.title)
                                .foregroundColor(.black)
                        })
                    }
                })
        }
        // prevent iPad split view
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear(){
            loadeData()
        }
    }
    
    func loadeData() {
        guard let url = URL(string: URL_CATEGORIES) else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode([Category].self, from: data) {
                    DispatchQueue.main.async {
                        self.categories = decodedResponse
                    }
                    return
                }
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            
        }.resume()
    }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView(categories: [Category.default], showMenu: .constant(false))
    }
}
