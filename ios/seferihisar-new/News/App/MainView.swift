//
//  MainView.swift
//  News
//
//  Created by Asil Arslan on 21.12.2020.
//

import SwiftUI
import StoreKit

struct MainView: View {
    
    
//    @StateObject var storeManager = StoreManager ()
    @Binding var showMenu : Bool
    
    var body: some View {
        
        TabView{
            HomeView(showMenu: $showMenu)
                .tabItem {
                    Image(systemName: "square.grid.2x2")
                    Text(LocalizedStringKey("Home"))
                }
                .tag(0)
            
            CategoryView(showMenu: $showMenu)
                .tabItem {
                    Image(systemName: "rectangle.grid.1x2")
                    Text(LocalizedStringKey("Category"))
                }
                .tag(1)
            
            FavoriteView(showMenu: $showMenu)
                .tabItem {
                    Image(systemName: "bookmark")
                    Text(LocalizedStringKey("Favorite"))
                }
                .tag(2)
            
            TagView(showMenu: $showMenu)
                .tabItem {
                    Image(systemName: "tag.circle")
                    Text(LocalizedStringKey("Tag"))
                }
                .tag(3)
            
            SettingView(showMenu: $showMenu)
                .tabItem {
                    Image(systemName: "slider.horizontal.3")
                    Text(LocalizedStringKey("Setting"))
                }
                .tag(4)
        }
//        .onAppear(perform: {
//            SKPaymentQueue.default().add(storeManager)
//            storeManager.getProducts(productIDs: IN_APP_PRODUCTS)
//        })
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(showMenu: .constant(false))
    }
}
