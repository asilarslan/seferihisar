//
//  HomeView.swift
//  News
//
//  Created by Asil Arslan on 21.12.2020.
//

import SwiftUI
import KingfisherSwiftUI


struct HomeView: View {
    @Namespace var animation
    @State var headlinePosts = [New]()
    @State var authors = [Author]()
    @State var posts = [New]()
    @State var categories = [Category]()
    @State var page = 1
    @State var isLoading = false
    @Binding var showMenu : Bool
    @State var selectedCategory : Category = Category.home
    
    //    var interstitial:Interstitial = Interstitial()
    
    var body: some View {
        
        NavigationView {
            List {
                
                if IS_CATEGORIES_VISIBLE {
                    Section{
                        ScrollView(.horizontal, showsIndicators: false, content: {
                            
                            HStack(){
                                TabButton(category: Category.home, selectedCategory: $selectedCategory, animation: animation)
                                    .onTabChanged{ (newCrop) in
                                        isLoading = false
                                        posts.removeAll()
                                        page = 1
                                        loadData()
                                    }
                                    .padding(.leading)
                                ForEach(categories){tab in
                                    
                                    // Tab Button...
                                    
                                    TabButton(category: tab, selectedCategory: $selectedCategory, animation: animation)
                                        .onTabChanged{ (category) in
                                            isLoading = false
                                            posts.removeAll()
                                            page = 1
                                            loadCategoryDatas(category:category)
                                        }
                                }
                            }
                        })
                        .listRowInsets(EdgeInsets())
                        .padding(.vertical)
                    }
                }
                
                
                if IS_HEADLINE_VISIBLE && selectedCategory.id == Category.home.id{
                    Section(header: HeaderView(text: "Headline")) {
                        
                        if HEADLINE_TYPE == .single {
                            ZStack {
                                // embed as hidden in ZStack to remove right arrow
                                NavigationLink(destination: NewsView(new: posts.first ?? New.default)) {
                                    //
                                }
                                .hidden()
                                NewsHeadlineView(post: headlinePosts.first ?? New.default)
                            }
                        }else{
                            GeometryReader { geometry in
                                ScrollView(.horizontal, showsIndicators: false) {
                                    HStack(spacing: 15) {
                                        ForEach(headlinePosts) { item in
                                            NavigationLink(
                                                destination: NewsView(new: item),
                                                label: {
                                                    
                                                    KFImage(source: .network(
                                                        item.jetpack_featured_media_url != nil && item.jetpack_featured_media_url != "" ?
                                                            URL(string: (item.jetpack_featured_media_url)!)!
                                                            :
                                                            URL(string: EMPTY_IMAGE_URL)!
                                                    ))
                                                    .resizable()
                                                    .scaledToFill()
                                                    .frame(width: geometry.size.width - 50, height: 300)
                                                    .cornerRadius(35)
                                                    .overlay(
                                                        VStack(alignment: .leading) {
                                                            Spacer()
                                                            TitleAndDateView(post: item)
                                                        }
                                                        .padding()
                                                        .background(LinearGradient(gradient: Gradient(colors: [Color("ColorHeadline"), Color.clear]), startPoint: .bottom, endPoint: .top).cornerRadius(35))
                                                    )
                                                })
                                        }
                                    }
                                    .padding(.horizontal)
                                }
                            }
                            .frame(height: 310).listRowInsets(EdgeInsets())
                        }
                    }
                }
                if selectedCategory.id == Category.home.id{
                    Section(header: HeaderView(text: "Authors")) {
                        GeometryReader { geometry in
                            ScrollView(.horizontal, showsIndicators: false) {
                                HStack(spacing: 15) {
                                    ForEach(authors) { item in
                                        NavigationLink(
                                            destination: AuthorNewsView(author: item),
                                            label: {
                                                
                                                KFImage(source: .network(
                                                    item.photo != nil && item.photo != "" ?
                                                        URL(string: (item.photo)!)!
                                                        :
                                                        URL(string: EMPTY_IMAGE_URL)!
                                                ))
                                                .resizable()
                                                .scaledToFill()
                                                .frame(width: (geometry.size.width - 65) / 2, height: 200)
                                                .cornerRadius(25)
                                                .overlay(
                                                    VStack(alignment: .leading) {
                                                        Spacer()
                                                        HStack {
                                                            VStack(alignment: .leading,spacing:10) {
                                                                Text("\(item.first_name) \(item.last_name)")
                                                                    .font(.body)
                                                                    .fontWeight(.bold)
                                                                    .foregroundColor(.white)
                                                                    .lineLimit(4)
                                                                
                                                                //                            Text(post.date.toDate())
                                                                //                                .font(.body)
                                                                //                                .foregroundColor(Color("ColorHeadlineDate"))
                                                            }
                                                            Spacer()
                                                        }
                                                    }
                                                    .padding()
                                                    .background(LinearGradient(gradient: Gradient(colors: [Color("ColorHeadline"), Color.clear]), startPoint: .bottom, endPoint: .top).cornerRadius(25))
                                                )
                                                
                                                
                                            })
                                    }
                                }
                                .padding(.horizontal)
                            }
                        }
                        .frame(height: 210).listRowInsets(EdgeInsets())
                    }
                }
                
                Section(header: HeaderView(text: "Latest")) {
                    ForEach (posts) { row in
                        NavigationLink(destination: NewsView(new: row)) {
                            NewsRowView(post: row)
                        }
                    }
                    HStack {
                        Image(systemName: "arrow.down")
                        Text(LocalizedStringKey("Scroll to new posts")).font(.footnote)
                            .onAppear {
                                if selectedCategory.id == Category.home.id {
                                    if !isLoading {
                                        self.page = self.page + 1
                                        loadData()
                                    }
                                }else{
                                    if !isLoading {
                                        self.page = self.page + 1
                                        loadeCategoryData()
                                    }
                                }
                                
                                print("Reached end of scroll view")
                            }
                    }
                }
            }
            .navigationBarTitle(LocalizedStringKey("Home"))
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: {
                        withAnimation{
                            
                            self.showMenu.toggle()
                        }
                    }, label: {
                        
                        Image(systemName: self.showMenu ? "xmark" : "line.horizontal.3")
                            .font(.title)
                            .foregroundColor(Color.black)
                    })
                    
                }
            })
        }
        // prevent iPad split view
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear(){
            loadHeadlineData()
            loadData()
            loadeCategoryData()
            loadAuthors()
            //            if !UserDefaults.standard.bool(forKey: "remove_ads") {
            //                self.interstitial.showAd()
            //            }
        }
    }
    
    
    
    func loadHeadlineData() {
        guard let url = URL(string: URL_HEADLINE_POSTS) else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode([New].self, from: data) {
                    DispatchQueue.main.async {
                        self.headlinePosts = decodedResponse
                    }
                    return
                }
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            
        }.resume()
    }
    
    func loadData() {
        isLoading = true
        guard let url = URL(string: "\(URL_POSTS)&page=\(page)") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        

            
            URLSession.shared.dataTask(with: request) {data, response, error in
                if let data = data {
                    do {
                       let decodedResponse = try JSONDecoder().decode([New].self, from: data)
                            DispatchQueue.main.async {
                                isLoading = false
                                self.posts.append(contentsOf: decodedResponse)
                                //                            self.isLoading = false
                            }
                    } catch let jsonError as NSError {
                        print("JSON decode failed: \(jsonError.localizedDescription)")
                    } catch DecodingError.keyNotFound(let key, let context) {
                        Swift.print("could not find key \(key) in JSON: \(context.debugDescription)")
                    } catch DecodingError.valueNotFound(let type, let context) {
                        Swift.print("could not find type \(type) in JSON: \(context.debugDescription)")
                    } catch DecodingError.typeMismatch(let type, let context) {
                        Swift.print("type mismatch for type \(type) in JSON: \(context.debugDescription)")
                    } catch DecodingError.dataCorrupted(let context) {
                        Swift.print("data found to be corrupted in JSON: \(context.debugDescription)")
                    } catch let error as NSError {
                        NSLog("Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)")
                    }
                    return
                }
                
                
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            
        }.resume()
    }
    
    func loadAuthors() {
        guard let url = URL(string: "http://seferihisar.com/mobile/api/get_authors.php") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            if let data = data {
                do {
                    let decodedResponse = try JSONDecoder().decode([Author].self, from: data)
                    DispatchQueue.main.async {
                        self.authors = decodedResponse
                    }
                } catch let jsonError as NSError {
                    print("JSON decode failed: \(jsonError.localizedDescription)")
                } catch DecodingError.keyNotFound(let key, let context) {
                    Swift.print("could not find key \(key) in JSON: \(context.debugDescription)")
                } catch DecodingError.valueNotFound(let type, let context) {
                    Swift.print("could not find type \(type) in JSON: \(context.debugDescription)")
                } catch DecodingError.typeMismatch(let type, let context) {
                    Swift.print("type mismatch for type \(type) in JSON: \(context.debugDescription)")
                } catch DecodingError.dataCorrupted(let context) {
                    Swift.print("data found to be corrupted in JSON: \(context.debugDescription)")
                } catch let error as NSError {
                    NSLog("Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)")
                }
                return
            }
            
            
            
        }.resume()
    }
    
    func loadCategoryDatas(category: Category) {
        isLoading = true
        guard let url = URL(string: "\(URL_CATEGORY_POST)\(category.id)&page=\(page)") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode([New].self, from: data) {
                    DispatchQueue.main.async {
                        isLoading = false
                        self.posts.append(contentsOf: decodedResponse)
                        //                            self.isLoading = false
                    }
                    return
                }
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            
        }.resume()
    }
    
    func loadeCategoryData() {
        guard let url = URL(string: URL_CATEGORIES) else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode([Category].self, from: data) {
                    DispatchQueue.main.async {
                        self.categories = decodedResponse
                    }
                    return
                }
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            
        }.resume()
    }
}


struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(authors: [Author.default], posts: [New.default, New.default], showMenu: .constant(false))
    }
}
