//
//  AuthorNewsView.swift
//  Seferihisar
//
//  Created by Asil Arslan on 29.12.2020.
//

import SwiftUI

struct AuthorNewsView: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @GestureState private var dragOffset = CGSize.zero
    var author: Author
    
    @State var posts : AuthorPosts = AuthorPosts.default
    @State var page = 1
    @State var isLoading = false
    
    var body: some View {
        List{
            ForEach (posts.posts) { row in
                NavigationLink(destination: NewsView(new: New(id: row.id, date: row.date.toDate3(), title: Rendered(rendered: row.title), content: Rendered(rendered: row.content), excerpt: Rendered(rendered: row.excerpt), jetpack_featured_media_url: nil, link: row.url))) {
                    AuthorPostView(aurhorPost: row)
                }
            }
            HStack {
                Image(systemName: "arrow.down")
                Text(LocalizedStringKey("Scroll to new posts")).font(.footnote)
                    .onAppear {
                        if !isLoading {
                            self.page = self.page + 1
                            loadData()
                        }
                        print("Reached end of scroll view")
                    }
            }
        }
        .navigationBarTitle("\(author.first_name) \(author.last_name)", displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        // add custom back button and share button
        .navigationBarItems(leading: Button(action : {
            self.mode.wrappedValue.dismiss()
        }){
            Image(systemName: "arrow.left")
        }
        )
        // enable swipe back
        .gesture(DragGesture().updating($dragOffset, body: { (value, state, transaction) in
            if (value.startLocation.x < 20 && value.translation.width > 100) {
                self.mode.wrappedValue.dismiss()
            }
        }))
        .onAppear(){
            loadData()
        }
    }
    
    
    func loadData() {
        isLoading = true
        guard let url = URL(string: "https://seferihisar.com/api/get_author_posts/?id=\(author.id)") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode(AuthorPosts.self, from: data) {
                    DispatchQueue.main.async {
                        isLoading = false
                        for post in decodedResponse.posts {
                            if !posts.posts.contains(where: { $0.id == post.id }) {
                                self.posts.posts.append(post)
                            }
                        }
                        
                        //                            self.isLoading = false
                    }
                    return
                }
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            
        }.resume()
    }
}

struct AuthorNewsView_Previews: PreviewProvider {
    static var previews: some View {
        AuthorNewsView(author: Author.default)
    }
}
