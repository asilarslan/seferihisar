//
//  String+Extensions.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 29.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation
import UIKit

extension String{
    
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: String.Encoding.unicode) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
   

    
    
}





