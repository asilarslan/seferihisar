//
//  Functions.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 21.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class Functions{
    
    static func stringFromHTML( htmlString: String?) -> NSAttributedString
    {
        let data = htmlString?.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        
        return attrStr!
    }
    
    
    static func convertDateFormatter(inputDate: String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone.local//NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.locale = NSLocale(localeIdentifier: "tr_TR") as Locale?
        let date = dateFormatter.date(from: inputDate)
        
        if date != nil {
            dateFormatter.dateFormat = "MMMM dd, yyyy"// HH:mm"///this is what you want to convert format
            dateFormatter.timeZone = NSTimeZone.local//NSTimeZone(name: "UTC") as TimeZone!
            dateFormatter.locale = NSLocale(localeIdentifier: "tr_TR") as Locale?
            let timeStamp = dateFormatter.string(from: date!)
            
            return timeStamp
        }else {
            return inputDate
        }
        
        
        
    }
    
    static func setBadge(){
        let defaults = UserDefaults.standard
        let badge = defaults.integer(forKey: "news_notification")
        
        setBadgeIndicator(badgeCount: badge)
    }
    
    static func setBadgeIndicator(badgeCount: Int) {
        let application = UIApplication.shared
        application.applicationIconBadgeNumber = badgeCount
    }

    
}
