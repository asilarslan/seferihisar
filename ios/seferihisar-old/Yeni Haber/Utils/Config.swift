//
//  Config.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation
import FirebaseInstanceID

class Config {
    static let adImage = "http://seferihisar.com/wp-content/uploads/2018/04/sah.jpg"
    static let adUrl = "http://sahtugrulinsaat.com"
    
    static var FirebaseToken : String? {
        return InstanceID.instanceID().token()
    }
}
