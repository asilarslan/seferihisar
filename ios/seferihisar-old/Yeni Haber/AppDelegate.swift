//
//  AppDelegate.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 21.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Firebase

import CoreData
import Firebase
import FirebaseInstanceID
import Alamofire
import SwiftyJSON

import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    weak var mainViewController : ViewController?
    var homeViewController:HomeViewController!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .lightContent
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "İptal"
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        //Messaging.messaging().delegate = self
        return true
    }
    
    
    
    
    
    fileprivate func updateFCMToken() {
        if let refreshedToken = InstanceID.instanceID().token() {
            let systemVersion = UIDevice.current.systemVersion
            print("iOS\(systemVersion)")
            
            //iPhone or iPad
            let model = UIDevice.current.model
            
            print("device type=\(model)")
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            print(deviceID)
            print("InstanceID token: \(refreshedToken)")
            
            
            let parameters: [String: String] = [
                "regid" : refreshedToken,
                "device_name" : model,
                "serial" : deviceID,
                "os_version" : systemVersion
            ]
            
            let headers = ["Content-Type": "application/json"]
            
            Alamofire.request("http://seferihisar.com/?api-fcm=register", method: .post,parameters:parameters, encoding: JSONEncoding.default, headers: headers).validate()
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        
                        let json = JSON(value)
                        print("Firebase noti")
                        print(json)
                        
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        updateFCMToken()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        
        
        let defaults = UserDefaults.standard
        let badge = defaults.integer(forKey: "news_notification")
        defaults.set(badge + 1, forKey: "news_notification")
        
        mainViewController?.showBadge()
        Functions.setBadge()
        
        switch application.applicationState {
        case .active:
            //app is currently active, can update badges count here
            print("app is currently active, can update badges count here")
            if let aps = userInfo[AnyHashable("gcm.notification.post_id")] {
                print(aps)
            }
            break
        case .inactive:
            print("app is transitioning from background to foreground (user taps notification), do what you need when user taps here")
            
            if let aps = userInfo[AnyHashable("gcm.notification.post_id")] {
                print(aps)
                if(homeViewController != nil){
                    homeViewController.showDetail(id: aps as! String)
                }else{
                    
                     let homeViewController = HomeViewController()
homeViewController.showDetail(id: aps as! String)
                }
            }
            
            
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            break
        case .background:
            print("app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here")
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            
            break
        default:
            break
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Refresh Token")
        updateFCMToken()
    }
    
    
    // Receive data message on iOS 10 devices while app is in the foreground.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("dataa")
        //NotificationCenterController.processMessage(remoteMessage.appData)
    }
    
}

