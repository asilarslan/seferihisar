//
//  AuthorPostTableViewCell.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 13.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class AuthorPostTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var avatar: UIImageView!
    @IBOutlet var nameSurname: UILabel!
    @IBOutlet var postDate: UILabel!
//    var authorId:Int!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        

        // Configure the view for the selected state
    }

}
