//
//  AuthorsViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class AuthorsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    @IBOutlet var tableView: UITableView!
    var posts = [Post]()
    var category: Category!
    var page = 1
    var author : Author!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        category = Category(id: 96, slug: "", title: "Yazarlar", description: "", parent: "", post_count: 0)

        self.title = author.first_name + " " + author.last_name
        getPosts(id: category.id, page: page)
        // Do any additional setup after loading the view.
    }
    
   
    fileprivate func getPosts(id : Int, page : Int){
        
        createIndicator()
        Alamofire.request("http://seferihisar.com/?json=get_author_posts&id=" + String(author.id) + "&page=" + String(page) + "&count=5", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    let json = JSON(value)
                    
                    for (_,subJson):(String, JSON) in json["posts"] {
                        
                        var images = [Image]()
                        
                        for (_,imgJson):(String, JSON) in subJson["attachments"] {
                            
                            let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                            
                            images.append(image)
                        }
                        
                        let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                        
                        var comments = [Comment]()
                        
                        for (_,commentJson):(String, JSON) in subJson["comments"] {
                            
                            let comment = Comment(id: commentJson["id"].intValue, name: commentJson["name"].stringValue, url: commentJson["url"].stringValue, date: commentJson["date"].stringValue, content: commentJson["content"].stringValue, parent: commentJson["parent"].intValue)
                            
                            comments.append(comment)
                        }
                        
                        let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: self.author.avatar, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments,author:self.author)
                        
                        self.posts.append(post)
                    }
                    
                    self.hideIndicator()
                    self.tableView.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let post = posts[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "AuthorTableViewCell", for: indexPath)as! AuthorTableViewCell
            cell.selectionStyle = .none
            
            
        
        //let imageUrl = post.thumbnail
        let imageUrl = author.avatar
            if imageUrl != nil && imageUrl != "" {
                let url = URL(string: imageUrl!)
                //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
                //cell.postImage.kf.setImage(with: resource)
                cell.authorImage.kf.setImage(with: url)
            }
            
            cell.postTitle.setTitleHTMLFromString(htmlText: post.title)
            cell.postContent.setHTMLFromString(htmlText: post.excerpt)
            cell.postDate.text = Functions.convertDateFormatter(inputDate: post.date)
            cell.postComment.text = String(post.comment_count)
            return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
            if !(indexPath.row + 1 < self.posts.count) {
                page = page + 1
                getPosts(id: category.id, page: page)
        }
        
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "showAuthorPostDetail",
            let destination = segue.destination as? PostViewController,
            let index = tableView.indexPathForSelectedRow?.row
        {
            destination.post = posts[index]
            //destination.delegate = self
        }
    }
    
//showAuthorPostDetail
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
