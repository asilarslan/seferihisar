//
//  AuthorTableViewCell.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit

class AuthorTableViewCell: UITableViewCell {
    @IBOutlet var authorImage: UIImageView!
    
    @IBOutlet var postTitle: UILabel!
    @IBOutlet var postContent: UILabel!
    @IBOutlet var postDate: UILabel!
    @IBOutlet var postComment: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
