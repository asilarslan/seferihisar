//
//  AuthorViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 12.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class AuthorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    
    @IBOutlet var tableView: UITableView!
    var authors = [Author]()
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    override func viewDidLoad() {
        super.viewDidLoad()

        getAuthors()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return authors.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let author = authors[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuthorPostTableViewCell", for: indexPath)as! AuthorPostTableViewCell
        cell.selectionStyle = .none
        
        let url = URL(string: author.avatar!)
            //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
            //cell.postImage.kf.setImage(with: resource)
        cell.avatar.kf.setImage(with: url)
        
        
        Alamofire.request("http://seferihisar.com/?json=get_author_posts&id=" + String(author.id) + "&count=1", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    let json = JSON(value)
                    
                    for (_,subJson):(String, JSON) in json["posts"] {
                        
                        var images = [Image]()
                        
                        for (_,imgJson):(String, JSON) in subJson["attachments"] {
                            
                            let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                            
                            images.append(image)
                        }
                        
                        
                        
                        
                        let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                        
                        var comments = [Comment]()
                        
                        
                        
                        let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: subJson["thumbnail"].stringValue, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments)
                            //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
                            //cell.postImage.kf.setImage(with: resource)
                        if author.avatar == nil || author.avatar == ""{
                            let imageUrl = post.thumbnail.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
                            if imageUrl != nil && imageUrl != "" {
                                let url = URL(string: imageUrl!)
                                cell.avatar.kf.setImage(with: url)
                            }else{
                                cell.avatar.image = UIImage(named:"AppIcon40x40")
                            }
                        }
                        
                        cell.title.setTitleHTMLFromString(htmlText: post.title)
                        cell.postDate.text = Functions.convertDateFormatter(inputDate: post.date)
                        
                    }
                    
                case .failure(let error):
                    print(error)
                }
        }
        
        cell.nameSurname.text = author.first_name + " " + author.last_name
        return cell
    }
    
    func getAuthors(){
        
        createIndicator()
        Alamofire.request("http://seferihisar.com/?json=get_author_index", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    let json = JSON(value)
                    
                     for (_,subJson):(String, JSON) in json["authors"] {
                        let author = Author(id: subJson["id"].intValue, slug: subJson["slug"].stringValue, first_name: subJson["first_name"].stringValue, last_name: subJson["last_name"].stringValue, nickname: subJson["nickname"].stringValue, avatar: "")
                        if author.id == 527 || author.id == 2  || author.id == 6 /* || author.id == 1021 */  {
                            self.getAuthorImage(author: author)
                            self.authors.append(author)
                            if author.id == 527 || author.id == 2  || author.id == 6   || author.id == 1021 {
                                   if author.id == 527 {
                                        author.avatar = "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png"
                                    }else if author.id == 2 {
                                        author.avatar = "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png"
                                    }else if author.id == 6 {
                                        author.avatar = "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png"
                                    }else if author.id == 1021 {
                                        author.avatar = "https://seferihisar.com/wp-content/uploads/2018/10/durmus-odabasi.png"
                                    }
                                    //self.authors.append(author)
                                }
                                
                            }
                        }
                        

                    
                        
                    
                    print(json)
                    self.tableView.reloadData()
                    
                    self.hideIndicator()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    
    fileprivate func getAuthorImage(author : Author) {
        
        
        Alamofire.request("http://seferihisar.com/mobile/get_user_data.php", method: .post, parameters: [ "user_id" :author.id]).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    if value != nil {
                        let json = JSON(value)
                        
                        if json["meta_value"] != nil{
                            author.avatar = json["meta_value"].stringValue
                            
                            if author.avatar != "" {
                                
                                //self.authors.append(author)
                                self.tableView.reloadData()
                            }
                        }
                    }
                    
                    
                    
                   
                    
                    
                case .failure(let error):
                    print(error)
                }
        }
        
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "showAuthorPostsDetail",
            let destination = segue.destination as? AuthorsViewController,
            let index = tableView.indexPathForSelectedRow?.row
        {
            destination.author = self.authors[index]
            //destination.delegate = self
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
