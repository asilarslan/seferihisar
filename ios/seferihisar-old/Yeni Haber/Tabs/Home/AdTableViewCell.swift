//
//  AdTableViewCell.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit

class AdTableViewCell: UITableViewCell {

    @IBOutlet var adImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
