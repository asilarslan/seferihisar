//
//  CommentViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 10.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit

class CommentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet var tableView: UITableView!
    var post:Post!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = post.title.convertHtml().string
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return post.comments.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        let cell = tableView.dequeueReusableCell(withIdentifier: "comment", for: indexPath)as! CommentTableViewCell
        cell.selectionStyle = .none
        let comment = post.comments[indexPath.row]
        cell.name.setHTMLFromString(htmlText:comment.name)
        cell.comment.setHTMLFromString(htmlText:comment.content)
        cell.date.text = Functions.convertDateFormatter(inputDate: comment.date)
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
