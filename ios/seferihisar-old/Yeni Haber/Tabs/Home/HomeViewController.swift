//
//  HomeViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 21.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher


class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    

    private let refreshControl = UIRefreshControl()
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    @IBOutlet var tableView: UITableView!
    var posts = [Post]()
    var searchPosts = [Post]()
    var originalPosts = [Post]()
    var headerPosts = [Post]()
    var page = 1
    var isSearch = false
    var headerPost: Post!
    let searchBar = UISearchBar()
    var titleView : UIView!
    
    var isHeaderOk = false
    var isAdOk = false
    var isPostsOk = false
    
    var photo:String!
    var ad:String!
    @IBOutlet var searchButton: UIBarButtonItem!
    @IBAction func searchButtonTapped(_ sender: Any) {
//        showSearchBar()
    }
    
//    func showSearchBar() {
//
//        isSearch = true
//        posts.removeAll()
//        tableView.reloadData()
//
//        searchBar.alpha = 0
//        navigationItem.titleView = searchBar
////        navigationItem.setLeftBarButtonItem(nil, animated: true)
//        navigationItem.rightBarButtonItem = nil
//        UIView.animate(withDuration: 0.5, animations: {
//            self.searchBar.alpha = 1
//        }, completion: { finished in
//            self.searchBar.becomeFirstResponder()
//        })
//    }
//
//    func hideSearchBar() {
//
//        isSearch = false
//        posts = originalPosts
//        tableView.reloadData()
//
//        navigationItem.rightBarButtonItem = searchButton
////        titleView.alpha = 0
//        searchBar.endEditing(true)
//        searchBar.setShowsCancelButton(false, animated: true)
//        UIView.animate(withDuration: 0.3, animations: {
//            self.navigationItem.titleView = self.titleView
////            self.titleView.alpha = 1
//        }, completion: { finished in
//
//        })
//    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    @objc func keyboardWillHide(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView = navigationItem.titleView
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Haber ara"
        searchBar.delegate = self
        
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.homeViewController = self
        
        
//        self.navigationItem.titleView = searchBar
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshPostData(_:)), for: .valueChanged)

        
        createIndicator()
        
        headerPosts.removeAll()
        posts.removeAll()
        
        getHeaderPosts()
        getAdDetails()
        getPosts(page: page)
        
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        hideSearchBar()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
            
            createIndicator()
        let params: [String: String] = ["search": searchBar.text!, "count": "20"]
            
            
            Alamofire.request("http://seferihisar.com/?json=get_search_results", method: .post,parameters: params).validate()
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        
                        self.posts.removeAll()
                        
                        let json = JSON(value)
                        
                        for (_,subJson):(String, JSON) in json["posts"] {
                            
                            var images = [Image]()
                            
                            for (_,imgJson):(String, JSON) in subJson["attachments"] {
                                
                                let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                                
                                images.append(image)
                            }
                            
                            let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                            var comments = [Comment]()
                            
                            for (_,commentJson):(String, JSON) in subJson["comments"] {
                                
                                let comment = Comment(id: commentJson["id"].intValue, name: commentJson["name"].stringValue, url: commentJson["url"].stringValue, date: commentJson["date"].stringValue, content: commentJson["content"].stringValue, parent: commentJson["parent"].intValue)
                                
                                comments.append(comment)
                            }
                            
                            let author = Author(id: subJson["author"]["id"].intValue, slug: subJson["author"]["slug"].stringValue, first_name: subJson["author"]["first_name"].stringValue, last_name: subJson["author"]["last_name"].stringValue, nickname: subJson["author"]["nickname"].stringValue, avatar: "")
                            
                            var thumbnail = subJson["thumbnail"].stringValue
                            
                     
                            
                            if author.id == 527 || author.id == 2  || author.id == 6  || author.id == 818  || author.id == 1018 {
                                if author.id == 527 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png"
                                    author.avatar = thumbnail
                                }else if author.id == 2 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png"
                                    author.avatar = thumbnail
                                }else if author.id == 6 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png"
                                    author.avatar = thumbnail
                                }else if author.id == 818 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png"
                                    author.avatar = thumbnail
                                }else if author.id == 1018 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/09/feyzi.png"
                                    author.avatar = thumbnail
                                }
                            }
                            
                            let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: subJson["thumbnail"].stringValue, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments,author:author)
                            self.posts.append(post)
                        }
                        
                        self.searchBar.endEditing(true)
                        self.hideIndicator()
                        self.tableView.reloadData()
                        
                    case .failure(let error):
                        print(error)
                    }
            }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchBar.text!.count >= 3 {

        createIndicator()
        let params: [String: String] = ["search": searchText, "count": "20"]


        Alamofire.request("http://seferihisar.com/?json=get_search_results", method: .post,parameters: params).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):

                    self.posts.removeAll()

                    let json = JSON(value)

                    for (_,subJson):(String, JSON) in json["posts"] {

                        var images = [Image]()

                        for (_,imgJson):(String, JSON) in subJson["attachments"] {

                            let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)

                            images.append(image)
                        }

                        let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                        var comments = [Comment]()

                        for (_,commentJson):(String, JSON) in subJson["comments"] {

                            let comment = Comment(id: commentJson["id"].intValue, name: commentJson["name"].stringValue, url: commentJson["url"].stringValue, date: commentJson["date"].stringValue, content: commentJson["content"].stringValue, parent: commentJson["parent"].intValue)

                            comments.append(comment)
                        }

                        let author = Author(id: subJson["author"]["id"].intValue, slug: subJson["author"]["slug"].stringValue, first_name: subJson["author"]["first_name"].stringValue, last_name: subJson["author"]["last_name"].stringValue, nickname: subJson["author"]["nickname"].stringValue, avatar: "")

                        var thumbnail = subJson["thumbnail"].stringValue
                    
//                        if author.id == 527 || author.id == 2  || author.id == 6  || author.id == 818   || author.id == 1018 {
//                            if author.id == 527 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 2 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 6 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 818 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 1018 {
//                               thumbnail = "http://seferihisar.com/wp-content/uploads/2018/09/feyzi.png"
//
//                                author.avatar = thumbnail
//                            }
//                        }

                        let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: subJson["thumbnail"].stringValue, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments,author:author)
                        self.getAuthorImage(post: post, author: author)
                        self.posts.append(post)
                    }

                    self.searchBar.endEditing(true)
                    self.hideIndicator()
                    self.tableView.reloadData()

                case .failure(let error):
                    print(error)
                }
            }}else{
            posts.removeAll()
            tableView.reloadData()
        }
        
    }
    
    @objc private func refreshPostData(_ sender: Any) {
        self.refreshControl.endRefreshing()
        //getHeaderPosts()
        isSearch = false
        isAdOk = false
        isHeaderOk = false
        isPostsOk = false
        getHeaderPosts()
        getAdDetails()
        getPosts(page: page)
    }
    
    fileprivate func getAdDetails(){
        Alamofire.request("http://seferihisar.com/mobile/ads.json", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    print(value)
                    let json = JSON(value)
                    self.photo = json["photoUrl"].stringValue
                    self.ad = json["adUrl"].stringValue
                    self.isAdOk = true
                    print("ad finished")
                    self.checkIsComplete()
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    fileprivate func getHeaderPosts(){
        
        Alamofire.request("http://seferihisar.com/?json=get_posts&custom_fields=swp_primaryshow&count=50", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    let json = JSON(value)
                    
                    for (_,subJson):(String, JSON) in json["posts"] {
                        
                        let sliderIsOn = subJson["custom_fields"]["swp_primaryshow"].arrayValue
                        
                        if sliderIsOn.contains("on") {
                            var images = [Image]()
                            
                            for (_,imgJson):(String, JSON) in subJson["attachments"] {
                                
                                let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                                
                                images.append(image)
                            }
                            
                            let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                            
                            var comments = [Comment]()
                            
                            for (_,commentJson):(String, JSON) in subJson["comments"] {
                                
                                let comment = Comment(id: commentJson["id"].intValue, name: commentJson["name"].stringValue, url: commentJson["url"].stringValue, date: commentJson["date"].stringValue, content: commentJson["content"].stringValue, parent: commentJson["parent"].intValue)
                                
                                comments.append(comment)
                            }
                            var thumbnail = subJson["thumbnail"].stringValue
                         
                            
                        
                            
                            let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: thumbnail, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments)
                            
                            if  self.headerPosts.count < 10 {
                                self.headerPosts.append(post)
                            }else{
                                break
                            }
                        }
                        
                    }
                    
                    print("header finished")
                    self.isHeaderOk = true
                    
                    self.checkIsComplete()
                case .failure(let error):
                    print(error)
                }
        }
       
    }
    
    fileprivate func getPosts(page : Int){
        
        //"http://seferihisar.com/?json=get_posts&exclude=content,categories,tags,custom_fields&page="
        Alamofire.request("http://seferihisar.com/?json=get_posts&exclude=categories,tags,custom_fields&page=" + String(page) + "&count=10", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    let json = JSON(value)
                    
                    for (_,subJson):(String, JSON) in json["posts"] {
                        
                        var images = [Image]()
                        
                        for (_,imgJson):(String, JSON) in subJson["attachments"] {
                            
                            let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                            
                            images.append(image)
                        }
                        
                        let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                        
                        var comments = [Comment]()
                        
                        for (_,commentJson):(String, JSON) in subJson["comments"] {
                            
                            let comment = Comment(id: commentJson["id"].intValue, name: commentJson["name"].stringValue, url: commentJson["url"].stringValue, date: commentJson["date"].stringValue, content: commentJson["content"].stringValue, parent: commentJson["parent"].intValue)
                            
                            comments.append(comment)
                        }
                        
                        let author = Author(id: subJson["author"]["id"].intValue, slug: subJson["author"]["slug"].stringValue, first_name: subJson["author"]["first_name"].stringValue, last_name: subJson["author"]["last_name"].stringValue, nickname: subJson["author"]["nickname"].stringValue, avatar: "")
                        
                        var thumbnail = subJson["thumbnail"].stringValue
                        
                     
//                        if author.id == 527 || author.id == 2  || author.id == 6  || author.id == 818  || author.id == 1018 {
//                            if author.id == 527 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 2 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 6 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 818 {
//                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png"
//                                author.avatar = thumbnail
//                            }else if author.id == 1018 {
//                                thumbnail =  "http://seferihisar.com/wp-content/uploads/2018/09/feyzi.png"
//                                author.avatar = thumbnail
//                            }
//                        }
                        
                        let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: thumbnail, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images, comments:comments,author:author)
                        self.getAuthorImage(post: post, author: author)

                        self.originalPosts.append(post)
                    }
                    self.posts = self.originalPosts
                    self.isPostsOk = true
                    
                    print("posts finished")
                    self.checkIsComplete()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    fileprivate func getAuthorImage(post:Post, author : Author) {
        
        
        Alamofire.request("http://seferihisar.com/mobile/get_user_data.php", method: .post, parameters: [ "user_id" :author.id]).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    if value != nil {
                        let json = JSON(value)
                        
                        if json["meta_value"] != ""{
                            author.avatar = json["meta_value"].stringValue
                            post.thumbnail = author.avatar
                            self.tableView.reloadData()
                        }
                    }
                    
                    
                    
                    
                    
                    
                case .failure(let error):
                    print(error)
                }
        }
        
    }
    
    func checkIsComplete(){
        if /*isHeaderOk && */isPostsOk /*&& isAdOk*/{
            self.hideIndicator()
            self.tableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearch {
            return 1
        }else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isSearch {
            return posts.count
        }else{
            if section == 0 {
                return 1
            }else if section == 1 {
                return 1
            }else{
                return posts.count
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearch {
            let post = posts[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath)as! PostTableViewCell
            cell.selectionStyle = .none
            
            
            let imageUrl = post.thumbnail.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
            if imageUrl != nil && imageUrl != "" {
                let url = URL(string: imageUrl!)
                print("*******")
                print(post.title)
                print(url)
                print("*******")
                cell.postImage.kf.setImage(with: url,placeholder: nil,
                    options: [.transition(ImageTransition.fade(1))],
                    progressBlock: { receivedSize, totalSize in
                        print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                    completionHandler: { image, error, cacheType, imageURL in
                        print("\(indexPath.row + 1): Finished")
                })
                //tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            }
            
            cell.title.setTitleHTMLFromString(htmlText: post.title)
            cell.excerpt.setHTMLFromString(htmlText: post.excerpt)
            cell.date.text = Functions.convertDateFormatter(inputDate: post.date)
            cell.numberOfComments.text = String(post.comment_count)
            return cell
        }else{
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath)as! HeaderTableViewCell
                cell.selectionStyle = .none
                cell.homeViewController = self
                cell.posts = headerPosts
                cell.update()
                //            cell.update(posts: headerPosts)
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdTableViewCell", for: indexPath)as! AdTableViewCell
                cell.selectionStyle = .none
//                let url = URL(string: Config.adImage)
                if photo != nil {
                    let url = URL(string: self.photo)
                    cell.adImage.kf.setImage(with: url)
                }
                return cell
            }else{
                let post = posts[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath)as! PostTableViewCell
                cell.selectionStyle = .none
                
                
                let imageUrl = post.thumbnail.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
                if imageUrl != nil && imageUrl != "" {
                    let url = URL(string: imageUrl!)
                    //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
                    //cell.postImage.kf.setImage(with: resource)
                    cell.postImage.kf.setImage(with: url)
                }
                
                cell.title.setTitleHTMLFromString(htmlText: post.title)
                cell.excerpt.setHTMLFromString(htmlText: post.excerpt)
                cell.date.text = Functions.convertDateFormatter(inputDate: post.date)
                cell.numberOfComments.text = String(post.comment_count)
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2 {
            if !(indexPath.row + 1 < self.posts.count) {
                createIndicator()
                page = page + 1
                getPosts(page: page)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
//            if let url = URL(string: Config.adUrl) {
//                UIApplication.shared.open(url, options: [:])
//            }
            if ad != nil {
                if let url = URL(string: ad) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "showPostDetail",
            let destination = segue.destination as? PostViewController,
            let index = tableView.indexPathForSelectedRow?.row
        {
            destination.post = posts[index]
            //destination.delegate = self
        }/*else if let viewController = segue.destination as? AddOrderViewController{
            viewController.delegate = self
            viewController.orderType = OrderType.add
        }else if  segue.identifier == showOrderDetail,
            let destination = segue.destination as? OrderDetailViewController{
            destination.order = selectedOrder
            destination.delegate = self
        }*/
    }
    
    func showDetail(id : String){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PostViewController") as! PostViewController
        newViewController.postId = id
        if(self.navigationController != nil) {
            self.navigationController?.pushViewController(newViewController, animated: true)
        }else{
//            let SB2 = UIStoryboard(name: "Main", bundle:nil)
//            let nvc = SB2.instantiateViewController(withIdentifier: "HomeNC") as! UINavigationController
//
//            nvc.pushViewController(newViewController, animated: true)
//            self.navigationController?.pushViewController(newViewController, animated: true)

//            self.present(newViewController, animated: true, completion: nil)
//            print("navigtioncontroller is nil")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
