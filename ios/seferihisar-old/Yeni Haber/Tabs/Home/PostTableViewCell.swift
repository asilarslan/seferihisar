//
//  PostTableViewCell.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 21.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet var postImage: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var excerpt: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var numberOfComments: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
