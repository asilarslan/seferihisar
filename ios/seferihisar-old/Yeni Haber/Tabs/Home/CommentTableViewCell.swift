//
//  CommentTableViewCell.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 10.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    @IBOutlet var name: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var comment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
