//
//  PostViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 29.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Photos

import Alamofire
import SwiftyJSON
import Kingfisher

class PostViewController: UIViewController {
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    @IBOutlet var postTitle: UILabel!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var postContent: UILabel!
    
    @IBOutlet var authorContent: UIView!
    @IBOutlet var authorImage: UIImageView!
    @IBOutlet var authorNameSurname: UILabel!
    var post : Post!
    var postId : String!
    @IBOutlet var photoGalleryContent: UIView!
    
    @IBOutlet var expert: UILabel!
    @IBOutlet var galleryTitle: UILabel!
    @IBOutlet var photoGallery: UIButton!
    @IBOutlet var sendCommentView: UIView!
    @IBOutlet var showCommentView: UIView!
    @IBOutlet var sendComment: UIImageView!
    @IBOutlet var showComment: UIImageView!
    fileprivate func updatePostDetail() {
        if post.images.count > 0 {
            photoGallery.isHidden = false
        }else{
            photoGallery.isHidden = true
        }
        galleryTitle.setWhiteHTMLFromString(htmlText: post.title)
        // photoGallery.setTitle(post.title,for: .normal)
       
        //expert.setHTMLFromString(htmlText:post.excerpt)
        postTitle.setWhiteHTMLFromString(htmlText: post.title)
        //postTitle.attributedText = post.title.convertHtml()
        var content = "<style>img{max-width:" + String(describing: self.view.frame.width) + ";height:auto;} figure{max-width:100%;height:auto;} iframe{width:100%;}</style>"
        content = content +  post.content
        postContent.clipsToBounds = true
        postContent.setHTMLFromString(htmlText:content)
      
        
        if post.thumbnail_images != nil {
            let imageUrl = post.thumbnail_images.medium_large.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
            if imageUrl != nil && imageUrl != "" {
                let url = URL(string: imageUrl)
                //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
                //cell.postImage.kf.setImage(with: resource)
                postImage.kf.setImage(with: url)
                authorContent.isHidden = true
            }else{
                
                if post.author.id == 527 || post.author.id == 2  || post.author.id == 6  || post.author.id == 818  || post.author.id == 1018 {
                let imageUrl = post.thumbnail.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
                if imageUrl != nil && imageUrl != "" {
                    let url = URL(string: imageUrl)
                    //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
                    //cell.postImage.kf.setImage(with: resource)
                    authorImage.kf.setImage(with: url)
                }
                authorContent.isHidden = false
                authorNameSurname.text = post.author.first_name + " " + post.author.last_name
                
                }
                
            }
        }else{
             if post.author.id == 527 || post.author.id == 2  || post.author.id == 6  || post.author.id == 818  || post.author.id == 1018 {
                let imageUrl = post.thumbnail.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
                if imageUrl != nil && imageUrl != "" {
                    let url = URL(string: imageUrl)
                    //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
                    //cell.postImage.kf.setImage(with: resource)
                    authorImage.kf.setImage(with: url)
                }
                
                authorContent.isHidden = false
                authorNameSurname.text = post.author.first_name + " " + post.author.last_name
            }
            }
           
    }
    
    @objc func addTapped(){
        
        
    }
    @objc func playTapped(){
        
        if post != nil {
            let activityViewController = UIActivityViewController(
                activityItems: [Functions.stringFromHTML(htmlString: post.title).string + " " + post.url],
                applicationActivities: nil)
            
            present(activityViewController, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        let play = UIBarButtonItem(title: "Paylaş", style: .plain, target: self, action: #selector(playTapped))
        
        navigationItem.rightBarButtonItems = [/*add, */play]
        
        postImage.clipsToBounds = true
        postImage.contentMode = .scaleAspectFill
        photoGallery.leftImage(image: #imageLiteral(resourceName: "default"), renderMode: .alwaysOriginal)
        if post != nil {
           
            updatePostDetail()
            
        }else{
            photoGallery.isHidden = true
            createIndicator()
            Alamofire.request("http://seferihisar.com/?json=get_post&id=" + postId , method: .post).validate()
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        
                        
                        let json = JSON(value)
                        
                        //for (_,subJson):(String, JSON) in json["posts"] {
                            
                            var images = [Image]()
                            
                            for (_,imgJson):(String, JSON) in json["post"]["attachments"] {
                                
                                let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                                
                                images.append(image)
                            }
                            
                            let thumbnail_images = Image(full: json["post"]["thumbnail_images"]["full"]["url"].stringValue, thumbnail: json["post"]["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: json["post"]["thumbnail_images"]["medium"]["url"].stringValue, medium_large: json["post"]["thumbnail_images"]["medium_large"]["url"].stringValue, large: json["post"]["thumbnail_images"]["large"]["url"].stringValue)
                            
                            var comments = [Comment]()
                            
                            for (_,commentJson):(String, JSON) in json["post"]["comments"] {
                                
                                let comment = Comment(id: commentJson["id"].intValue, name: commentJson["name"].stringValue, url: commentJson["url"].stringValue, date: commentJson["date"].stringValue, content: commentJson["content"].stringValue, parent: commentJson["parent"].intValue)
                                
                                comments.append(comment)
                            }
                            
                            let author = Author(id: json["post"]["author"]["id"].intValue, slug: json["post"]["author"]["slug"].stringValue, first_name: json["post"]["author"]["first_name"].stringValue, last_name: json["post"]["author"]["last_name"].stringValue, nickname: json["post"]["author"]["nickname"].stringValue, avatar: "")
                            
                            var thumbnail = json["post"]["thumbnail"].stringValue
                            
                            if author.id == 527 || author.id == 2  || author.id == 6  || author.id == 818  || author.id == 1018 {
                                if author.id == 527 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png"
                                    author.avatar = thumbnail
                                }else if author.id == 2 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png"
                                    author.avatar = thumbnail
                                }else if author.id == 6 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png"
                                    author.avatar = thumbnail
                                }else if author.id == 818 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png"
                                    author.avatar = thumbnail
                                }else if author.id == 1018 {
                                    thumbnail = "http://seferihisar.com/wp-content/uploads/2018/09/feyzi.png"
                                    author.avatar = thumbnail
                                }
                            }
                            
                        self.post = Post(id: json["post"]["id"].intValue, type: json["post"]["type"].stringValue, slug: json["post"]["slug"].stringValue, url: json["post"]["url"].stringValue, status: json["post"]["status"].stringValue, title: json["post"]["title"].stringValue, title_plain: json["post"]["post"]["title_plain"].stringValue, content: json["post"]["content"].stringValue, excerpt: json["post"]["excerpt"].stringValue, date: json["post"]["date"].stringValue, modified: json["post"]["modified"].stringValue, thumbnail: thumbnail, comment_count: json["post"]["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images, comments:comments,author:author)
                            self.hideIndicator()
                        
                        print("posts finished")
                        //self.checkIsComplete()
                        
                        self.updatePostDetail()
                        
                    case .failure(let error):
                        print(error)
                    }
            }
        }
        
        let sendCommentTap = UITapGestureRecognizer(target: self, action: #selector(PostViewController.sendCommentTapped))
        sendCommentView.addGestureRecognizer(sendCommentTap)
        sendCommentView.isUserInteractionEnabled = true
        
        
        let showCommentTap = UITapGestureRecognizer(target: self, action: #selector(PostViewController.showCommentTapped))
        showCommentView.addGestureRecognizer(showCommentTap)
        showCommentView.isUserInteractionEnabled = true
        
        sendComment.image = sendComment.image?.imageWithColor(color1: UIColor.white)
        showComment.image = showComment.image?.imageWithColor(color1: UIColor.white)

    }
    
    
    @IBAction func showPhotoGallery(_ sender: Any) {
        gotoVC()
    }
    
    func gotoVC() {
        DispatchQueue.main.async(execute: { () -> Void in
            let mainView = PhotoGalleryViewController()
            let images = self.post.images
            var imageArray = [UIImage]()
            
            for var image in images {
                let url = URL(string: image.medium_large)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                let image = UIImage(data: data!)
                
                imageArray.append(image!)
            }
            mainView.imageArray = imageArray
            mainView.title = self.post.title.convertHtml().string
           
            self.navigationController?.pushViewController(mainView, animated: true)
        })
    }
    
    @objc func sendCommentTapped()
    {
        print("sendCommentTapped")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "SendCommentViewController") as! SendCommentViewController
        newViewController.post = post
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @objc func showCommentTapped()
    {
        print("showCommentTapped")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        newViewController.post = post
        self.navigationController?.pushViewController(newViewController, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? AuthorsViewController{
          viewController.author = self.post.author
         }
        
    }
    //showAuthorPostsFromAuthorPostDetail
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
