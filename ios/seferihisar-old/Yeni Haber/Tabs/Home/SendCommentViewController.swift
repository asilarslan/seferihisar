//
//  SendCommentViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 10.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class SendCommentViewController: UIViewController {
    
    @IBOutlet var nameLabel: UITextField!
    @IBOutlet var epostaLabel: UITextField!
    @IBOutlet var commetnTextView: UITextView!
    var post:Post!
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = post.title.convertHtml().string
        commetnTextView.placeholder = "Yorumunuz.."
        commetnTextView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        commetnTextView.layer.borderWidth = 1.0
        commetnTextView.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    
    @IBAction func btnSendTapped(_ sender: Any) {
        createIndicator()
        
        let name = nameLabel.text
        let email = epostaLabel.text
        let content = commetnTextView.text
        
        if (name?.isEmpty)! || (email?.isEmpty)! || (content?.isEmpty)! {
            // create the alert
            let alert = UIAlertController(title: "Hata", message: "Lüütfen tüm alanları doldurunuz", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }else{
            let parameters: [String: String] = [
                "post_id" : String(post.id),
                "name" : name!,
                "email" : email!,
                "content" : content!
            ]
            
            Alamofire.request("http://seferihisar.com/?json=respond/submit_comment", method: .post,parameters:parameters).validate()
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        
                        let json = JSON(value)
                        self.hideIndicator()
                        _ = self.navigationController?.popViewController(animated: true)
                        
                    case .failure(let error):
                        print(error)
                    }
            }
        }
        
       
        
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
