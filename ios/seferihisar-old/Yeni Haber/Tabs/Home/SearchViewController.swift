//
//  SearchViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 3.06.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate  {
    var posts = [Post]()
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    @IBOutlet var tableView: UITableView!
    let searchBar = UISearchBar()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Haber ara"
        searchBar.delegate = self
        navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
       return posts.count
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        createIndicator()
        let params: [String: String] = ["search": searchBar.text!, "count": "20"]
        
        
        Alamofire.request("http://seferihisar.com/?json=get_search_results", method: .post,parameters: params).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    self.posts.removeAll()
                    
                    let json = JSON(value)
                    
                    for (_,subJson):(String, JSON) in json["posts"] {
                        
                        var images = [Image]()
                        
                        for (_,imgJson):(String, JSON) in subJson["attachments"] {
                            
                            let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                            
                            images.append(image)
                        }
                        
                        let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                        var comments = [Comment]()
                        
                        for (_,commentJson):(String, JSON) in subJson["comments"] {
                            
                            let comment = Comment(id: commentJson["id"].intValue, name: commentJson["name"].stringValue, url: commentJson["url"].stringValue, date: commentJson["date"].stringValue, content: commentJson["content"].stringValue, parent: commentJson["parent"].intValue)
                            
                            comments.append(comment)
                        }
                        
                        let author = Author(id: subJson["author"]["id"].intValue, slug: subJson["author"]["slug"].stringValue, first_name: subJson["author"]["first_name"].stringValue, last_name: subJson["author"]["last_name"].stringValue, nickname: subJson["author"]["nickname"].stringValue, avatar: "")
                        
                        var thumbnail = subJson["thumbnail"].stringValue
                        
                        
                        
                        if author.id == 527 {
                            thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png"
                            author.avatar = thumbnail
                        }else if author.id == 2 {
                            thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png"
                            author.avatar = thumbnail
                        }else if author.id == 6 {
                            thumbnail = "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png"
                            author.avatar = thumbnail
                        }else if author.id == 818 {
                            thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png"
                            author.avatar = thumbnail
                        }else if author.id == 1018 {
                            thumbnail = "http://seferihisar.com/wp-content/uploads/2018/09/feyzi.png"
                            author.avatar = thumbnail
                        }
                        
                        
                        
                        let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: subJson["thumbnail"].stringValue, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments,author:author)
                        
                        if author.id == 527 || author.id == 2  || author.id == 6  || author.id == 818  || author.id == 1018 {
                            post.thumbnail = author.avatar
                            
                        }
                        self.posts.append(post)
                    }
                    
                    self.searchBar.endEditing(true)
                    self.hideIndicator()
                    self.tableView.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = posts[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath)as! PostTableViewCell
        cell.selectionStyle = .none
        
        
        let imageUrl = post.thumbnail
        if imageUrl != nil && imageUrl != "" {
            let url = URL(string: imageUrl!)
            print("*******")
            print(post.title)
            print(url)
            print("*******")
            cell.postImage.kf.setImage(with: url,placeholder: nil,
                                       options: [.transition(ImageTransition.fade(1))],
                                       progressBlock: { receivedSize, totalSize in
                                        print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
            },
                                       completionHandler: { image, error, cacheType, imageURL in
                                        print("\(indexPath.row + 1): Finished")
            })
            //tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
        
        cell.title.setTitleHTMLFromString(htmlText: post.title)
        cell.excerpt.setHTMLFromString(htmlText: post.excerpt)
        cell.date.text = Functions.convertDateFormatter(inputDate: post.date)
        cell.numberOfComments.text = String(post.comment_count)
        return cell
        
    }
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "showSearchPostDetail",
            let destination = segue.destination as? PostViewController,
            let index = tableView.indexPathForSelectedRow?.row
        {
            destination.post = posts[index]
            //destination.delegate = self
        }/*else if let viewController = segue.destination as? AddOrderViewController{
         viewController.delegate = self
         viewController.orderType = OrderType.add
         }else if  segue.identifier == showOrderDetail,
         let destination = segue.destination as? OrderDetailViewController{
         destination.order = selectedOrder
         destination.delegate = self
         }*/
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
