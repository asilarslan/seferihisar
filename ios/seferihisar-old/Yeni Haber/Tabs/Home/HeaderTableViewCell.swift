//
//  HeaderTableViewCell.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 21.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Kingfisher

import Alamofire
import SwiftyJSON


class HeaderTableViewCell: UITableViewCell, UIScrollViewDelegate  {
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet var btn5: UIButton!
    @IBOutlet var btn6: UIButton!
    @IBOutlet var btn7: UIButton!
    @IBOutlet var btn8: UIButton!
    @IBOutlet var btn9: UIButton!
    @IBOutlet var btn10: UIButton!
    @IBOutlet var buttonsView: UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    var posts = [Post]()
    var currentPost : Post!
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    var homeViewController : HomeViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        buttonsView.isHidden = true
        btn1.setTitleColor(UIColor.white, for: UIControl.State.selected)
        btn2.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn3.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn4.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn5.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn6.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn7.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn8.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn9.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn10.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        
        scrollView.delegate = self
        
        scrollView.showsVerticalScrollIndicator = false
        
        scrollView.isDirectionalLockEnabled = true
        
        scrollView.frame = self.frame
        
//        createIndicator()
//        //"http://seferihisar.com/?json=get_tag_posts&tag_slug=manset"
//        Alamofire.request("http://seferihisar.com/?json=get_posts&custom_fields=swp_primaryshow&count=50", method: .post).validate()
//            .responseJSON { response in
//                switch response.result {
//                case .success(let value):
//
//print("xxx 1")
//                    let json = JSON(value)
//
//                    for (_,subJson):(String, JSON) in json["posts"] {
//
//                        let sliderIsOn = subJson["custom_fields"]["swp_primaryshow"].arrayValue
//
//                        if sliderIsOn.contains("on") {
//                            var images = [Image]()
//
//                            for (_,imgJson):(String, JSON) in subJson["attachments"] {
//
//                                let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
//
//                                images.append(image)
//                            }
//
//                            let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
//
//                            var comments = [Comment]()
//
//                            let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: subJson["thumbnail"].stringValue, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments)
//
//                            if  self.posts.count < 10 {
//                                self.posts.append(post)
//                            }else{
//                                break
//                            }
//                        }
//
//                    }
//print("xxx 2")
//                    var count = 0
//                    for post in self.posts {
//                        let xPosition = self.frame.width * CGFloat(count)
//                        let view = UIView(frame: CGRect(x: xPosition, y: 170, width: self.frame.width, height: 110))
//                        let imageView = UIImageView(frame: CGRect(x: xPosition, y: 0, width: self.frame.width, height: 220))
//                        let label = UILabel(frame: CGRect(x: xPosition + 8, y: 140, width: self.frame.width - 8, height: 100))
//                        view.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.5)
//                        label.setTitleHTMLFromString(htmlText: post.title)
//                        label.numberOfLines = 0;
//                        label.lineBreakMode = .byWordWrapping
//                        label.textColor = UIColor.white
//                        let url = URL(string: post.thumbnail_images.medium_large!)
//                        imageView.kf.setImage(with: url)
//                        imageView.contentMode = .scaleAspectFill
////                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(post:)))
////                        imageView.isUserInteractionEnabled = true
////                        imageView.addGestureRecognizer(tapGestureRecognizer)
//
//                        let tap = UITapGestureRecognizer(target: self, action: #selector(HeaderTableViewCell.tappedMe))
//                        imageView.addGestureRecognizer(tap)
//                        imageView.isUserInteractionEnabled = true
//
//                        self.scrollView.addSubview(imageView)
//                        self.scrollView.addSubview(view)
//                        self.scrollView.addSubview(label)
//                        self.scrollView.contentSize.width = self.scrollView.frame.width * CGFloat(count + 1)
//                        count = count + 1
//                    }
//
//                    print("xxx 3")
//
//                    self.hideIndicator()
//
//                    if self.posts.count > 0 {
//                        self.currentPost = self.posts[0]
//                    }
//
//                case .failure(let error):
//                    print(error)
//                }
//        }
       
    }
    
    @objc func tappedMe()
    {
        print("Tapped on Image")
        openPost(post: currentPost)
    }
    
//    @objc func imageTapped(post: Post)
//    {
//       openPost(post: post)
//    }
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }

    func update(){
        //buttonsView.isHidden = false
        var count = 0
        self.scrollView.contentSize.width = self.scrollView.frame.width * CGFloat(self.posts.count)
        for post in self.posts {
            let xPosition = screenWidth * CGFloat(count)
            let parentView = UIView(frame: CGRect(x: xPosition, y: 0, width: screenWidth, height: 220))
            let view = UIView(frame: CGRect(x: 0, y: 170, width: self.frame.width, height: 110))
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width+50, height: 220))
            let label = UILabel(frame: CGRect(x:  8, y: 140, width: self.frame.width - 8, height: 100))
            view.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.5)
            label.setTitleHTMLFromString(htmlText: post.title)
            label.numberOfLines = 2;
            label.lineBreakMode = .byWordWrapping
            label.textColor = UIColor.white
            let url = URL(string: post.thumbnail_images.medium_large.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!)
            imageView.kf.setImage(with: url)
            imageView.contentMode = .scaleAspectFill
            //                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(post:)))
            //                        imageView.isUserInteractionEnabled = true
            //                        imageView.addGestureRecognizer(tapGestureRecognizer)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(HeaderTableViewCell.tappedMe))
            imageView.addGestureRecognizer(tap)
            imageView.isUserInteractionEnabled = true
            
            parentView.addSubview(imageView)
            parentView.addSubview(view)
            parentView.addSubview(label)
            scrollView.addSubview(parentView)
            count = count + 1
        }
        

        
        
        if self.posts.count >= 10{
            showButtons()
        }
        
        self.hideIndicator()
        
        if self.posts.count > 0 {
            self.currentPost = self.posts[0]
        }
    }
    
    func openPost(post: Post)  {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PostViewController") as! PostViewController
//        newViewController.post = post
//        homeViewController.present(newViewController, animated: true, completion: nil)
    
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "PostViewController") as! PostViewController
                newViewController.post = post
        homeViewController.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if posts.count == 10 {
            if scrollView.contentOffset.y>0 {
                scrollView.contentOffset.y = 0
            }
            if scrollView.contentOffset.x < self.frame.width {
                setSelected(btn: btn1)
                currentPost = posts[0]
            }else if scrollView.contentOffset.x >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 2 {
                setSelected(btn: btn2)
                currentPost = posts[1]
            }else if scrollView.contentOffset.x * 2 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 3 {
                setSelected(btn: btn3)
                currentPost = posts[2]
            }else if scrollView.contentOffset.x * 3 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 4 {
                setSelected(btn: btn4)
                currentPost = posts[3]
            }else if scrollView.contentOffset.x * 4 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 5 {
                setSelected(btn: btn5)
                currentPost =  posts[4]
            }else if scrollView.contentOffset.x * 5 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 6 {
                setSelected(btn: btn6)
                currentPost =  posts[5]
            }else if scrollView.contentOffset.x * 6 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 7 {
                setSelected(btn: btn7)
                currentPost =  posts[6]
            }else if scrollView.contentOffset.x * 7 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 8 {
                setSelected(btn: btn8)
                currentPost = posts[7]
            }else if scrollView.contentOffset.x * 8 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 9 {
                setSelected(btn: btn9)
                currentPost =  posts[8]
            }else if scrollView.contentOffset.x * 9 >= self.frame.width  && scrollView.contentOffset.x < self.frame.width * 10 {
                setSelected(btn: btn10)
                currentPost =  posts[9]
            }
        }
    }
    
    func setSelected (btn: UIButton){
        btn1.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn2.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn3.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn4.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn5.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn6.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn7.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn8.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn9.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn10.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
    }
    
    func goToPoint(startingPointForView : Double) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.scrollView.contentOffset.x = CGFloat(startingPointForView)
            }, completion: nil)
        }
    }
  
    @IBAction func btn1Tapped(_ sender: Any) {
        goToPoint(startingPointForView: 0)
        setSelected(btn: btn1)
    }
    
    @IBAction func btn2Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width))
        setSelected(btn: btn2)
    }
    
    @IBAction func btn3Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 2))
        setSelected(btn: btn3)
    }
    
    @IBAction func btn4Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 3))
        setSelected(btn: btn4)
    }
    
    @IBAction func btn5Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 4))
        setSelected(btn: btn5)
    }
    
    @IBAction func btn6Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 5))
        setSelected(btn: btn6)
    }
    
    @IBAction func btn7Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 6))
        setSelected(btn: btn7)
    }
    
    @IBAction func btn8Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 7))
        setSelected(btn: btn8)
    }
    
    @IBAction func btn9Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 8))
        setSelected(btn: btn9)
    }
    
    @IBAction func btn10Tapped(_ sender: Any) {
        goToPoint(startingPointForView: Double(self.frame.width * 9))
        setSelected(btn: btn10)
    }
    
    
    func createIndicator(){
        self.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: self.frame.size.width*0.5, y: self.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    func showButtons(){
        buttonsView.isHidden = false
    }
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
