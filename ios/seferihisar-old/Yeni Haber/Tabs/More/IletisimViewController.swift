//
//  IletisimViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol SelectTypeDelegate {
    func selectType(type: String)
}

class IletisimViewController: UIViewController ,SelectTypeDelegate{
    func selectType(type: String) {
        howText = type
        how.setTitle(type, for: .normal)
    }
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    @IBOutlet var date: UILabel!
    @IBOutlet var content: UILabel!
    
    @IBOutlet var nameSurname: UITextField!
    @IBOutlet var eposta: UITextField!
    @IBOutlet var how: UIButton!
    var howText:String!
    @IBOutlet var contactDetails: UITextView!
    
    @IBOutlet var send: UIButton!
    
    @IBAction func sendTapped(_ sender: Any) {
        
        let nameSurnameText = nameSurname.text
        let epostaText = eposta.text
        let contactDetailText = contactDetails.text
        
        howText = "test"
        
        if (nameSurnameText?.isEmpty)! || (epostaText?.isEmpty)! || (howText?.isEmpty)! || (contactDetailText?.isEmpty)! {
            
            let alert = UIAlertController(title: "Hata", message: "Lüütfen tüm alanları doldurunuz", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }else{
            let parameters: [String: String] = [
                "namesurname" : nameSurnameText!,
                "email" : epostaText!,
                "subject" : howText!,
                "message" : contactDetailText!
            ]
            
            
            
            Alamofire.request("http://seferihisar.com/mobile/send_mail.php", method: .post,parameters:parameters).validate()
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        
                        let json = JSON(value)
                        self.hideIndicator()
                        
                        
                       // _ = self.navigationController?.popViewController(animated: true)
                        
                    case .failure(let error):
                        print(error)
                    }
            }
            let alert = UIAlertController(title: "Mesajınız Gönderildi", message: "Masajınız Gönderilmiştir.", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactDetails.placeholder = "Mesajınız.."
        
        contactDetails.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        contactDetails.layer.borderWidth = 1.0
        contactDetails.layer.cornerRadius = 5
        
        createIndicator()
        Alamofire.request("http://seferihisar.com/?json=get_page&id=1252", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let page = Page(id: json["page"]["id"].intValue, type: json["page"]["type"].stringValue, slug: json["page"]["slug"].stringValue, url: json["page"]["url"].stringValue, status: json["page"]["status"].stringValue, title: json["page"]["title"].stringValue, title_plain: json["page"]["title_plain"].stringValue, content: json["page"]["content"].stringValue, excerpt: json["page"]["excerpt"].stringValue, date: json["page"]["date"].stringValue)
                    
                    self.date.text = Functions.convertDateFormatter(inputDate: page.date)
                    self.content.setHTMLFromString(htmlText: page.content)
                    self.hideIndicator()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? SelectTypeTableViewController{
         viewController.delegate = self
         }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
