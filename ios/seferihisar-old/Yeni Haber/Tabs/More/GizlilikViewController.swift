//
//  GizlilikViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class GizlilikViewController: UIViewController {

    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    @IBOutlet var date: UILabel!
    @IBOutlet var content: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createIndicator()
        Alamofire.request("http://seferihisar.com/?json=get_page&id=3150", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let page = Page(id: json["page"]["id"].intValue, type: json["page"]["type"].stringValue, slug: json["page"]["slug"].stringValue, url: json["page"]["url"].stringValue, status: json["page"]["status"].stringValue, title: json["page"]["title"].stringValue, title_plain: json["page"]["title_plain"].stringValue, content: json["page"]["content"].stringValue, excerpt: json["page"]["excerpt"].stringValue, date: json["page"]["date"].stringValue)
                    
                    self.date.text = Functions.convertDateFormatter(inputDate: page.date)
                    self.content.setHTMLFromString(htmlText: page.content)
                    self.hideIndicator()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
