//
//  CategoriesViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 22.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class CategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource   {
    
    @IBOutlet var tableView: UITableView!
    var categories = [Category]()
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getCategories()
    }
    
    fileprivate func getCategories(){
        
        createIndicator()
        Alamofire.request("http://seferihisar.com/?json=get_category_index", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    let json = JSON(value)
                    
                    for (_,subJson):(String, JSON) in json["categories"] {
                        
                        let category = Category(id: subJson["id"].intValue, slug: subJson["slug"].stringValue, title: subJson["title"].stringValue, description: subJson["description"].stringValue, parent: subJson["parent"].stringValue, post_count: subJson["post_count"].intValue)
                        
                   
                        self.categories.append(category)
                    }
                    
                    self.hideIndicator()
                    self.tableView.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category = categories[indexPath.row]
        //let post = posts[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath)as! CategoryTableViewCell
        cell.selectionStyle = .none
        
        cell.name.text = category.title
        cell.count.text = String(category.post_count)
        
        
        /*let imageUrl = post.thumbnail
        if imageUrl != nil && imageUrl != "" {
            let url = URL(string: imageUrl!)
            //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
            //cell.postImage.kf.setImage(with: resource)
            cell.postImage.kf.setImage(with: url)
        }
        
        cell.title.setTitleHTMLFromString(htmlText: post.title)
        cell.excerpt.setHTMLFromString(htmlText: post.excerpt)
        cell.date.text = Functions.convertDateFormatter(inputDate: post.date)
        cell.numberOfComments.text = String(post.comment_count)*/
        return cell
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "showCategoryPosts",
            let destination = segue.destination as? CategoryPostsViewController,
            let index = tableView.indexPathForSelectedRow?.row
        {
            destination.category = categories[index]
            //destination.delegate = self
        }/*else if let viewController = segue.destination as? AddOrderViewController{
         viewController.delegate = self
         viewController.orderType = OrderType.add
         }else if  segue.identifier == showOrderDetail,
         let destination = segue.destination as? OrderDetailViewController{
         destination.order = selectedOrder
         destination.delegate = self
         }*/
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
