//
//  CategoryPostsViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class CategoryPostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var tableView: UITableView!
    
    var category: Category!
    var posts = [Post]()
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    var page = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if category != nil {
            self.title = category.title
            posts.removeAll()
            getPosts(id: category.id, page: page)
        }
        
    }
    
    fileprivate func getPosts(id : Int, page : Int){
        
        createIndicator()
        Alamofire.request("http://seferihisar.com/?json=get_category_posts&exclude=categories,tags,comments,custom_fields&id=" + String(id) + "&page=" + String(page) + "&count=5", method: .post).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    let json = JSON(value)
                    
                    for (_,subJson):(String, JSON) in json["posts"] {
                        
                        var images = [Image]()
                        
                        for (_,imgJson):(String, JSON) in subJson["attachments"] {
                            
                            let image = Image(full: imgJson["images"]["full"]["url"].stringValue, thumbnail: imgJson["images"]["thumbnail"]["url"].stringValue, medium: imgJson["images"]["medium"]["url"].stringValue, medium_large: imgJson["images"]["medium_large"]["url"].stringValue, large: imgJson["images"]["large"]["url"].stringValue)
                            
                            images.append(image)
                        }
                        
                        
                        
                        
                        let thumbnail_images = Image(full: subJson["thumbnail_images"]["full"]["url"].stringValue, thumbnail: subJson["thumbnail_images"]["thumbnail"]["url"].stringValue, medium: subJson["thumbnail_images"]["medium"]["url"].stringValue, medium_large: subJson["thumbnail_images"]["medium_large"]["url"].stringValue, large: subJson["thumbnail_images"]["large"]["url"].stringValue)
                        var comments = [Comment]()
                        
                        let author = Author(id: subJson["author"]["id"].intValue, slug: subJson["author"]["slug"].stringValue, first_name: subJson["author"]["first_name"].stringValue, last_name: subJson["author"]["last_name"].stringValue, nickname: subJson["author"]["nickname"].stringValue, avatar: "")
                        
                        var thumbnail = subJson["thumbnail"].stringValue
                        
                        if author.id == 527 || author.id == 2  || author.id == 6  || author.id == 818  || author.id == 1018 {
                            if author.id == 527 {
                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/kostebek.png"
                            }else if author.id == 2 {
                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/m-karabulut.png"
                            }else if author.id == 6 {
                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/05/tevfik-tortamis.png"
                            }else if author.id == 818 {
                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/04/mutlu-tuncer.png"
                            }else if author.id == 1018 {
                                thumbnail = "http://seferihisar.com/wp-content/uploads/2018/09/feyzi.png"
                            }
                        }
                        
                        let post = Post(id: subJson["id"].intValue, type: subJson["type"].stringValue, slug: subJson["slug"].stringValue, url: subJson["url"].stringValue, status: subJson["status"].stringValue, title: subJson["title"].stringValue, title_plain: subJson["title_plain"].stringValue, content: subJson["content"].stringValue, excerpt: subJson["excerpt"].stringValue, date: subJson["date"].stringValue, modified: subJson["modified"].stringValue, thumbnail: thumbnail, comment_count: subJson["comment_count"].intValue, images: images, thumbnail_images: thumbnail_images,comments:comments)
                        
                        self.posts.append(post)
                    }
                    
                    self.hideIndicator()
                    self.tableView.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return posts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = posts[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath)as! PostTableViewCell
            cell.selectionStyle = .none
            
            
            
            let imageUrl = post.thumbnail
            if imageUrl != nil && imageUrl != "" {
                let url = URL(string: imageUrl!)
                //let resource = ImageResource(downloadURL: url, cacheKey: "my_cache_key")
                //cell.postImage.kf.setImage(with: resource)
                cell.postImage.kf.setImage(with: url)
            }
            
            cell.title.setTitleHTMLFromString(htmlText: post.title)
            cell.excerpt.setHTMLFromString(htmlText: post.excerpt)
            cell.date.text = Functions.convertDateFormatter(inputDate: post.date)
            cell.numberOfComments.text = String(post.comment_count)
            return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if category != nil {
            if indexPath.section == 1 {
                if !(indexPath.row + 1 < self.posts.count) {
                    page = page + 1
                    getPosts(id: category.id, page: page)
                    
                }
            }
        }
        
        
    }
    
    func createIndicator(){
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: view.frame.size.width*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.startAnimating() // Start animating
    }
    
    
    func hideIndicator(){
        
        activityIndicator.stopAnimating() // On response stop animating
        activityIndicator.removeFromSuperview() // remove the view
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "showCategoryPostDetail",
            let destination = segue.destination as? PostViewController,
            let index = tableView.indexPathForSelectedRow?.row
        {
            destination.post = posts[index]
            //destination.delegate = self
        }
    }
    
    
//showCategoryPostDetail
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
