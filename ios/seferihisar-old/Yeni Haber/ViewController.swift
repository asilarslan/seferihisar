//
//  ViewController.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 9.06.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import UIKit

class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.mainViewController = self
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }   
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showBadge(){
        let defaults = UserDefaults.standard
        let badge = defaults.integer(forKey: "news_notification")
        if badge != 0 {
            self.tabBar.items?[0].badgeValue = String(badge)
            
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if item == self.tabBar.items?[0] {
            let defaults = UserDefaults.standard
            defaults.set(0, forKey: "news_notification")
            self.tabBar.items?[0].badgeValue = nil
            
            Functions.setBadge()
        }
        
        
        
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
