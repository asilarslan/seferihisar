//
//  Post.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 21.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation

class Post{
    
    var id: Int!
    var type: String!
    var slug: String!
    var url: String!
    var status: String!
    var title: String!
    var title_plain: String!
    var content: String!
    var excerpt: String!
    var date: String!
    var modified: String!
    var thumbnail: String!
    var comment_count: Int!
    var images = [Image]()
    var thumbnail_images :Image!
    var comments = [Comment]()
    var author:Author!
    
    init(id: Int,type: String, slug: String, url: String, status: String, title: String, title_plain: String, content: String, excerpt: String, date: String, modified: String,thumbnail: String, comment_count: Int, images : [Image], thumbnail_images: Image, comments : [Comment]) {
        self.id = id
        self.slug = slug
        self.url = url
        self.status = status
        self.title = title
        self.title_plain = title_plain
        self.content = content
        self.excerpt = excerpt
        self.date = date
        self.modified = modified
        self.thumbnail = thumbnail
        self.comment_count = comment_count
        self.images = images
        self.thumbnail_images = thumbnail_images
        self.comments = comments
    }
    
    
    init(id: Int,type: String, slug: String, url: String, status: String, title: String, title_plain: String, content: String, excerpt: String, date: String, modified: String,thumbnail: String, comment_count: Int, images : [Image], thumbnail_images: Image, comments : [Comment],author:Author) {
        self.id = id
        self.slug = slug
        self.url = url
        self.status = status
        self.title = title
        self.title_plain = title_plain
        self.content = content
        self.excerpt = excerpt
        self.date = date
        self.modified = modified
        self.thumbnail = thumbnail
        self.comment_count = comment_count
        self.images = images
        self.thumbnail_images = thumbnail_images
        self.comments = comments
        self.author = author
    }
    
    init(id: Int,type: String, slug: String, url: String, status: String, title: String, title_plain: String, content: String, excerpt: String, date: String, modified: String,thumbnail: String, comment_count: Int) {
        self.id = id
        self.slug = slug
        self.url = url
        self.status = status
        self.title = title
        self.title_plain = title_plain
        self.content = content
        self.excerpt = excerpt
        self.date = date
        self.modified = modified
        self.thumbnail = thumbnail
        self.comment_count = comment_count
    }
}
