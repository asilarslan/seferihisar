//
//  DeviceInfo.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 26.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation

class Info{
    
    var regid:String!
    var serial:String!
    var device_name:String!
    var os_version:String!
    
    init(regid:String,serial:String,device_name:String,os_version:String) {
        
        self.regid = regid
        self.serial = serial
        self.device_name = device_name
        self.os_version = os_version
    }
}
