//
//  Category.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation

class Category{
    var id : Int!
    var slug: String!
    var title: String!
    var description: String!
    var parent: String!
    var post_count:Int!
    
    init(id : Int, slug: String,title: String ,description: String,parent: String,post_count:Int) {
        self.id = id
        self.slug = slug
        self.title = title
        self.description = description
        self.parent = parent
        self.post_count = post_count
    }
}
