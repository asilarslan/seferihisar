//
//  Image.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 22.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation

class Image{
    var full: String!
    var thumbnail: String!
    var medium: String!
    var medium_large: String!
    var large: String!
    
    init(full: String, thumbnail: String, medium: String, medium_large: String, large: String) {
        self.full = full
        self.thumbnail = thumbnail
        self.medium = medium
        self.medium_large = medium_large
        self.large = large
    }
}
