//
//  Author.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 12.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation

class Author{
    
    var id: Int!
    var slug: String!
    var first_name: String!
    var last_name: String!
    var nickname: String!
    var avatar: String!
    
    init(id: Int, slug: String, first_name: String , last_name: String, nickname: String, avatar:  String) {
        self.id = id
        self.slug = slug
        self.first_name = first_name
        self.last_name = last_name
        self.nickname = nickname
        self.avatar = avatar
    }
    
}
