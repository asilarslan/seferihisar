//
//  Page.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 30.04.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation

class Page{
    
    var id: Int!
    var type: String!
    var slug: String!
    var url: String!
    var status: String!
    var title: String!
    var title_plain: String!
    var content: String!
    var excerpt: String!
    var date: String!
    
    init(id: Int,type: String, slug: String, url: String, status: String, title: String, title_plain: String, content: String, excerpt: String, date: String) {
        self.id = id
        self.slug = slug
        self.status = status
        self.title = title
        self.title_plain = title_plain
        self.content = content
        self.excerpt = excerpt
        self.date = date
    }
    
}
