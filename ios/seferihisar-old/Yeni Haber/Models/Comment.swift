//
//  Comment.swift
//  Yeni Haber
//
//  Created by Asil Arslan on 7.05.2018.
//  Copyright © 2018 Asil Arslan. All rights reserved.
//

import Foundation

class Comment{
    
    var id: Int!
    var name: String!
    var url: String!
    var date: String!
    var content: String!
    var parent:  Int!
    
    init(id: Int, name: String, url: String , date: String, content: String, parent:  Int) {
        self.id = id
        self.name = name
        self.url = url
        self.date = date
        self.content = content
        self.parent = parent
    }
    
}
